define(['jquery'], function($){

    $(document)
    .on('focus', 'input.number', function(){
        $(this).select();
        return false;
    })
    .on('change', 'input.number', function(){
        var $self = $(this);
        
        var precision = $self.data('precision') || 0;
		var val = $self.val();
		
        if (precision > 0) {
			val = parseFloat(val) || 0;
			val = val.toFixed(precision);
		}
		else {
			val = parseInt(val) || 0;
		}
        
		$self.val(val);
    })
    ;

	return $;
});