define(function (require, exports, module) {

    exports.pieCircle = function (option) {

        //区域
        var width  = 770;
        var height = 500;

        //中心点
        var cx = 205;
        var cy = 255;

        //半径，宽度
        var r = 45;
        var w = 30;
        //总数
        var total = option.total  ? option.total : 0;

        //监控
        var online = option.online ? option.online : 0;

        //使用中
        var working = option.working  ? option.working : 0;

        //待机
        var wait = option.wait  ? option.wait : 0;

        //故障
        var down = option.down  ? option.down : 0;


        //初始化 svgns
        var svgns = "http://www.w3.org/2000/svg";
        var chat = document.createElementNS(svgns, "svg:svg");
        chat.setAttribute("width", width);
        chat.setAttribute("height", height);
        chat.setAttribute("viewBox", "0 0 " + width + " " + height);

        //画圆环函数
        var drawCircle = function (option) {

            //参数
            var chat      = option.chat;
            var startangle = option.star ? option.star : 0;
            var endangle   = option.end ? option.end / 360 * Math.PI * 2 : Math.PI * 2;
            var r1         = option.r1;
            var r2         = option.r2;
            var color      = option.color ? option.color : "#ddd";
            var cx         = option.cx;
            var cy         = option.cy;
            var name       = option.name;
            var value      = option.value;
            var font_color = option.font_color ? option.font_color : "#ddd";

            //大于360度
            if (endangle >= Math.PI * 2) {
                if (name && value <= 0) {
                    option.end = 1;
                }
                else {
                    option.end = 359.99;
                }
                return drawCircle(option);
            }

            //圆环
            var startx1 = cx + r2 * Math.sin(startangle);
            var starty1 = cy - r2 * Math.cos(startangle);
            var x1      = cx + r1 * Math.sin(startangle);
            var y1      = cy - r1 * Math.cos(startangle);
            var x2      = cx + r1 * Math.sin(endangle);
            var y2      = cy - r1 * Math.cos(endangle);
            var endx2   = cx + r2 * Math.sin(endangle);
            var endy2   = cy - r2 * Math.cos(endangle);
            var big     = (endangle - startangle > Math.PI) ? 1 : 0;//big为1时，画大弧；big为0时，画小弧
            var path    = document.createElementNS(svgns, "path");
            var d       = "M " + startx1 + "," + starty1 + " L " + x1 + "," + y1 + " A " + r1 + "," + r1 + " 0 " + big + " 1 " + x2 + "," + y2 + " L " + endx2 + "," + endy2 + " A " + r2 + "," + r2 + " 0 " + big + " 0 " + startx1 + "," + starty1 + " Z";
            path.setAttribute("d", d);
            path.setAttribute("fill", color);
            path.setAttribute("stroke-width", "0");
            chat.appendChild(path);

            if (!name) {
                return chat;
            }
            //标签
            var label = document.createElementNS(svgns, "text");

            label.setAttribute("x", x1 - 36);
            label.setAttribute("y", y1 - 10);
            label.setAttribute("style", "font-size:12px");
            label.setAttribute("fill", font_color);
            label.appendChild(document.createTextNode(name));
            chat.appendChild(label);

            //数值
            // if (value <= 0) {
            //     return chat;
            // }

            var l_x=  (x1+startx1)/2;
            var l_y=  (y1+starty1)/2;
            var label = document.createElementNS(svgns, "text");
            label.setAttribute("x", l_x);
            label.setAttribute("y", l_y+10);
            label.setAttribute("style", "font-size:12px");
            label.setAttribute("fill","#033333");
            label.setAttribute("text-anchor", "middle");
            label.setAttribute("dominant-baseline", "middle");
            label.setAttribute("transform", "rotate(" + option.end + "," + x1 + " " + (y1 + r1) + ") rotate(-90,"+l_x+" "+l_y+ ")");
            label.appendChild(document.createTextNode(value));
            chat.appendChild(label);
            return chat;
        }

        //中心区域 - 圆
        var circle = document.createElementNS(svgns, "circle");
        circle.setAttribute("cx", cx);
        circle.setAttribute("cy", cy);
        circle.setAttribute("r", r);
        circle.setAttribute("fill", "#dfdfdf");
        chat.appendChild(circle);

        //中心区域 - 图片
        var image          = document.createElementNS(svgns, "image");
        image.href.baseVal = option.imageUrl || '/assets/img/dlmu/c-online.png';
        image.setAttribute("x", cx - 8);
        image.setAttribute("y", cy - 11);
        image.setAttribute("height", "22px");
        image.setAttribute("width", "16px");
        chat.appendChild(image);

        //使用中
        drawCircle({
            chat: chat,
            cx: cx,
            cy: cy,
            r1: r + 2 + ( 2 + w) * 2,
            r2: r + 2 + ( 2 + w) * 2 + w,
            end: working / online * 360,
            value: working,
            name: '使用',
            color: 'url(#grad-working)',
            font_color: '#990207',
            font_family: 'MicrosoftYaHei',
        })
        ;

        //待机
        drawCircle({
            chat: chat,
            cx: cx,
            cy: cy,
            r1: r + 2 + ( 2 + w),
            r2: r + 2 + ( 2 + w) + w,
            end: wait / online * 360,
            value: wait,
            name: '待机',
            color: 'url(#grad-wait)',
            font_color: '#7eddfe',
            font_family: 'MicrosoftYaHei',
        });

        //故障
        drawCircle({
            chat: chat,
            cx: cx,
            cy: cy,
            r1: r + 2,
            r2: r + 2 + w,
            end: down / online * 360,
            value: down,
            name: '故障',
            color: 'url(#grad-down)',
            font_color: '#f09819',
            font_family: 'MicrosoftYaHei',
        });

        //最外层的圆环 - 总数
        var r4 = r + 2 + (w + 2) * 3;
        drawCircle({
            chat: chat,
            cx: cx,
            cy: cy,
            r1: r4,
            r2: r4 + 5,
            end: 360,
            color: '#ddd',
        });

        //最外层的圆环上面的圆环Y
        var w_r  = 30;
        var w_w  = 5;
        var w_cx = cx + (r4 + 5) * Math.sin(305 / 360 * Math.PI * 2);
        var w_cy = cy - (r4 + 5) * Math.cos(305 / 360 * Math.PI * 2);
        drawCircle({
            chat: chat,
            cx: w_cx,
            cy: w_cy,
            r1: w_r,
            r2: w_r + w_w,
            end: 360,
            color: '#ddd',
        });

        //最外层的圆环上面的圆环 - 背景
        var circle = document.createElementNS(svgns, "circle");
        circle.setAttribute("cx", w_cx);
        circle.setAttribute("cy", w_cy);
        circle.setAttribute("r", w_r);
        circle.setAttribute("fill", "#fff");
        chat.appendChild(circle);

        //最外层的圆环上面的圆环 - 文字
        var label = document.createElementNS(svgns, "text");
        label.setAttribute("x", w_cx);
        label.setAttribute("y", w_cy + 8);
        label.setAttribute("text-anchor", "middle");
        label.setAttribute("style", "font-size:22px;font-weight:bold");
        label.setAttribute("fill", "#009696");
        label.appendChild(document.createTextNode(total));
        chat.appendChild(label);
        return chat;
    }
});