<?php

namespace Model;

class Theme extends \Gini\View {

    public function __construct($name, $vars=NULL)
    {

		parent::__construct($name, $vars);
	}

    private $_ob_cache;
    public function __toString()
    {
        if ($this->_ob_cache !== null) {
            return $this->_ob_cache;
        }

        $path = $this->_path;

        $theme = \Gini\Config::get('theme.default');
        $themePath = "@$theme/$path";

        $locale = \Gini\Config::get('system.locale');
        $localeThemePath = "@$theme/@$locale/$path";
        $localeSpecificPath = "@$locale/$path";

        $engines = \Gini\Config::get('view.engines');
        if (isset($GLOBALS['gini.view_map'][$localeThemePath])) {
        	$realPath = $GLOBALS['gini.view_map'][$localeThemePath];
            $engine = $engines[pathinfo($realPath, PATHINFO_EXTENSION)];
        } elseif (isset($GLOBALS['gini.view_map'][$localeSpecificPath])) {
            $realPath = $GLOBALS['gini.view_map'][$localeSpecificPath];
            $engine = $engines[pathinfo($realPath, PATHINFO_EXTENSION)];
        } elseif (isset($GLOBALS['gini.view_map'][$themePath])) {
            $realPath = $GLOBALS['gini.view_map'][$themePath];
            $engine = $engines[pathinfo($realPath, PATHINFO_EXTENSION)];
        } elseif (isset($GLOBALS['gini.view_map'][$path])) {
            $realPath = $GLOBALS['gini.view_map'][$path];
            $engine = $engines[pathinfo($realPath, PATHINFO_EXTENSION)];
        } else {
            foreach ($engines as $ext => $engine) {
                $realPath = \Gini\Core::locatePharFile(VIEW_DIR, "$localeThemePath.$ext");
                if (!$realPath) {
                    $realPath = \Gini\Core::locatePharFile(VIEW_DIR, "$localeSpecificPath.$ext");
                }
                if (!$realPath) {
                    $realPath = \Gini\Core::locatePharFile(VIEW_DIR, "$themePath.$ext");
                }
                if (!$realPath) {
                    $realPath = \Gini\Core::locatePharFile(VIEW_DIR, "$path.$ext");
                }
                if ($realPath) {
                    break;
                }
            }
        }
        if ($engine && $realPath) {
            $class = '\Gini\View\\'.$engine;
            $output = \Gini\IoC::construct($class, $realPath, $this->_vars);
        }
        return $this->_ob_cache = (string) $output;
    }

	public static function factory($name, $vars=NULL)
	{

		$basename = basename($name);

		if ( $basename == $name ) {

			$class_name = '\Model\Theme\\'.$name;

			if (class_exists($class_name)) {
				return \Gini\IoC::construct($class_name, $vars);
			}
		}

		return \Gini\IoC::construct('\Model\Theme', $name, $vars);
	}
}
