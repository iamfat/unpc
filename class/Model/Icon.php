<?php

namespace Model {
    
    class Icon {
        
        private $_object;
        
        function __construct($object) {
            $this->_object = $object;
        }
        
        private function _find_in($base, $paths) {
            foreach ($paths as $path) {
                if (file_exists($base . '/'.$path)) {
                    return $path;
                }
            }
        }
        
        function url($size='large') {
            $o = $this->_object;
            
            // 确保尺寸只能是以下几种
            assert(in_array($size, ['small', 'medium', 'large']));
            $path = $this->_find_in(APP_PATH . '/web', array(
                'cache/icon/'.$o->name().'-'.$o->id.'-'.$size.'.png',
                'assets/icon/'.$o->name().'-'.$size.'.png',
            ));    
            return URL($path ?: 'assets/icon/unknown-'.$size.'.png');
        }
        
        // $user->icon()->upload($file);
        function upload($file) {
            $o = $this->_object;
            $prefix = APP_PATH . 'web/cache/icon/'.$o->name().'-'.$o->id;
            \Gini\File::ensureDir(dirname($prefix));
            
        }
    }
    
}