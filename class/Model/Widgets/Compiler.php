<?php

namespace Model\Widgets {


    class Compiler extends \Model\Widget
    {
        function __construct ($vars = NULL)
        {
            parent::__construct('compiler', $vars);
        }

        //编译颜色
        public  function UpdateStyle ()
        {
            $web_css_dir = APP_PATH . '/web/assets/css/';
            $template_less = APP_PATH . '/' . RAW_DIR . '/less/inc/template.less';
            $vars_less = APP_PATH . '/' . RAW_DIR . '/less/inc/bootstrap/vars.less';
            $template_str = @file_get_contents($template_less);
            $colors = (array)Hub('config.design.color');
            $nav_settings = (array)Hub('config.nav');

            if ($nav_settings['hasShadow']) {
                if ($nav_settings['shadowPosition'] == 'bottom') {
                    $navBottomBorder = '4px solid ' . $colors['activeBottomBorder'];
                    $navTopBorder = 'none';
                } else {
                    $navTopBorder = '4px solid ' . $colors['activeTopBorder'];
                    $navBottomBorder = 'none';
                }
            } else {
                $navBottomBorder = 'none';
                $navTopBorder = 'none';
            }

            $bg = $colors['bg'] ?: \Gini\Config::get('config.bg');
            if ($filename = Hub('background.filename')) {
                $u = URL("!background/{$filename}");
                $bg .= " url($u) ";
                switch (Hub('background.repeat')) {
                    case 'x':
                        $bg .= ' repeat-x ';
                        break;
                    case 'y':
                        $bg .= ' repeat-y ';
                        break;
                    case 'xy':
                        $bg .= ' repeat ';
                        break;
                    default:
                        $bg .= ' no-repeat ';
                        break;
                }
                switch (Hub('background.position')) {
                    case 'left-bottom':
                        $bg .= ' left bottom ';
                        break;
                    case 'right-bottom':
                        $bg .= ' right bottom ';
                        break;
                    default:
                        $bg .= ' left top ';
                        break;
                }
            }

            $template_str = strtr($template_str, [
                '%navBg' => $colors['navBg'] ?: \Gini\Config::get('config.navBg'),
                '%navFont' => $colors['navFont'] ?: \Gini\Config::get('config.navFont'),
                '%navBgActive' => $colors['navBgActive'] ?: \Gini\Config::get('config.navBgActive'),
                '%navFontActive' => $colors['navFontActive'] ?: \Gini\Config::get('config.navFontActive'),
                '%bg' => $bg,
                '%font' => $colors['font'] ?: \Gini\Config::get('config.font'),
                '%mainTitle' => $colors['mainTitle'] ?: \Gini\Config::get('config.mainTitle'),
                '%leftTop' => $nav_settings['leftTop'] ? '10px' : '0px',
                '%rightTop' => $nav_settings['rightTop'] ? '10px' : '0px',
                '%rightBottom' => $nav_settings['rightBottom'] ? '10px' : '0px',
                '%leftBottom' => $nav_settings['leftBottom'] ? '10px' : '0px',
                '%navTopBorder' => $navTopBorder,
                '%navBottomBorder' => $navBottomBorder,
                '%activeBottomBorder' => $navBottomBorder,
                '%activeTopBorder' => $navTopBorder,
                '%layoutBg' => $colors['layoutBgColor'] ?: '#FFFFFF',
                '%moduleTitleColor' => $colors['moduleTitleColor'] ?: '#000000',
                '%bodyPrecent' => $colors['activeBodyPrecent'] ?: '10%'
            ]);

            @file_put_contents($vars_less, $template_str);

            //1. 更新目标源文件
            $src_path = APP_PATH . '/' . RAW_DIR . '/less/site.less';
            $dst_path = $web_css_dir . 'site.css';
            \Gini\File::delete($dst_path);
            $command = sprintf('lessc %s %s %s', escapeshellarg($src_path), escapeshellarg($dst_path), '--clean-css="--s1 --advanced --compatibility=ie8"'
            );
            exec($command);

            $src_path = APP_PATH . '/' . RAW_DIR . '/less/category.less';
            $dst_path = $web_css_dir . 'category.css';
            \Gini\File::delete($dst_path);
            $command = sprintf('lessc %s %s %s', escapeshellarg($src_path), escapeshellarg($dst_path), '--clean-css="--s1 --advanced --compatibility=ie8"'
            );
            exec($command);


            //2.更新layout中的图片信息
            exec('/usr/local/share/composer/vendor/iamfat/gini/bin/gini UnpcSystem UpdateStyle');

            return true;
        }

    }
}