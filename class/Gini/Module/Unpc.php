<?php

namespace Gini\Module {

    class Unpc {

        static function setup() {

            date_default_timezone_set(\Gini\Config::get('system.timezone') ?: 'Asia/Shanghai');
            class_exists('\Gini\Those');
            class_exists('\Gini\ThoseIndexed');
            _G('system.host', $_SERVER['HTTP_HOST']);

            if ($whiteList = \Gini\Config::get('host.whiteList')) {
                if (!is_array($whiteList)) {
                    $whiteList = [$whiteList];
                }
                if (!in_array(_G('system.host'), $whiteList)) {
                    header($_SERVER["SERVER_PROTOCOL"]." 403 Forbidden");
                    header("Status: 403 Forbidden");
                    die('Status: 403 Forbidden!');
                }
            }

            $path_prefix = preg_replace('/([^\/])$/', '$1/', dirname($_SERVER['SCRIPT_NAME']));
            _G('system.path_prefix', $path_prefix);

            // 获得当前的用户名, 设置全局变量ME
            $username = \Gini\Auth::userName();

            // OAuth based SSO support
            if (!$username && PHP_SAPI != 'cli' && $_GET['oauth-sso']) {
                $oauth = \Gini\IoC::construct('\Gini\OAuth\Client', $_GET['oauth-sso']);
                $username = $oauth->getUserName();

                if ($username) {
                    \Gini\Auth::login($username);
                }
            }

            $me = a('user', $username ? ['username'=>$username] : null);

            _G('ME', $me);

            setlocale(LC_MONETARY, \Gini\Config::get('system.locale') ?: 'zh_CN');
            \Gini\I18N::setup();

            /*
            * Unpc 开启默认自动模块建设功能
            * _G('MODS')合并默认+系统自定义增加的模块
            */

            $modules = those('category')
                ->whose('parent_id')->is(0)
                ->orWhose('parent_id')->is(NULL)
                ->orderBy('weight');

            _G('MODS', $modules);

            // $default_modules = (array) \Gini\Config::get('module.default');

            // foreach ($default_modules as $key => $info) {
            //     $modules->prepend((object)[
            //             'id' => $key,
            //             'name' => $info[name],
            //             'identity' => $info[key],
            //             'icon' => $info[icon],
            //             'weight' => $key
            //         ]);
            // }

        }

        static function shutdown() {

        }

        static function rememberMe() {

            $username = \Gini\Auth::userName();
            if ($username) {
                $now = time();

                //cookie有效期增加到30天
                if (ini_get('session.use_cookies')) {
                    setcookie(session_name().'_lt', session_id(), $now + 259200, \Gini\Config::get('system.session_path'), \Gini\Config::get('system.session_domain'));
                }

                //记录login状态
                $db = \Gini\Database::db();
                $db->adjustTable('_remember_me', array(
                        'fields' => array(
                            'id' => array('type'=>'char(40)', 'null'=>false, 'default'=>''),
                            'username' => array('type'=>'varchar(120)', 'null'=>false, 'default'=>''),
                            'mtime' => array('type'=>'int', 'null'=>false, 'default'=>0),
                        ),
                        'indexes' => array(
                            'PRIMARY' => array('type'=>'primary', 'fields'=>array('id')),
                            'mtime' => array('fields'=>array('mtime')),
                        )
                    )
                );

                $exp_time = $now - 259200;
                $db->query('DELETE FROM "_remember_me" WHERE "mtime"<?', null, [$exp_time]);
                $db->query('REPLACE INTO "_remember_me" ("id", "username", "mtime") VALUES (:id, :username, :mtime)',
                    null, [':id'=>session_id(), ':username'=>$username, ':mtime'=>$now]);
            }

        }

        public static function adminACL($e, $user, $action, $obj, $when, $where)
        {
            if (!$user->id) return $e->pass();
            if (!$user->admin) return $e->pass();
            return true;
        }

        /**
         * @param $html
         * @return mixed
         * 过滤url中的非法字符串
         */
        public static function safe_html($html)
        {
            $del = [
               "/<script.*>(.*)<\/script>/siU",
               '/on(click|dblclick|mousedown|mouseup|mouseover|mousemove|mouseout|keypress|keydown|keyup)="[^"]*"/i',
               '/on(abort|beforeunload|error|load|move|resize|scroll|stop|unload)="[^"]*"/i',
               '/on(blur|change|focus|reset|submit)="[^"]*"/i',
               '/on(bounce|finish|start)="[^"]*"/i',
               '/on(beforecopy|beforecut|beforeeditfocus|beforepaste|beforeupdate|contextmenu|cut)="[^"]*"/i',
               '/on(drag|dragdrop|dragend|dragenter|dragleave|dragover|dragstart|drop|losecapture|paste|select|selectstart)="[^"]*"/i',
               '/on(afterupdate|cellchange|dataavailable|datasetchanged|datasetcomplete|errorupdate|rowenter|rowexit|rowsdelete|rowsinserted)="[^"]*"/i',
                '/on(afterprint|beforeprint|filterchange|help|propertychange|readystatechange)="[^"]*"/i',
                '/javascript\:.*(\;|")/',
                '/to[S|s]tring/',
            ];
            $replace = ['$1','','','','','','','','',''];

            return preg_replace($del, $replace, $html);
        }

    }

}


namespace {

    /*
     * Shortcut for Hub ORM variables in Gini
     *
     **/
    if (function_exists('Hub')) {
        die('Hub() was declared by other libraries, which may cause problems!');
    } else {
        function Hub($key, $value = null)
        {
            $hub = a('hub', ['key' => $key]);
            if (is_null($value)) {
                return $hub->val ? @unserialize($hub->val) : null;
            } else {
                $hub->key = $key;
                $hub->val = @serialize($value);
                return $hub->save();
            }
        }
    }

    if (function_exists('Safe')) {
        die('Safe() was declared by other libraries, which may cause problems!');
    }
    else {
        function Safe($html)
        {
            return \Gini\Module\Unpc::safe_html($html);
        }
    }

    if (function_exists('U')) {
        die('U() was declared by other libraries, which may cause problems!');
    }
    else {
        function U($path, $vars = null)
        {
            return \Gini\IoC::construct('\Model\Theme', $path, $vars);
        }
    }

    if (function_exists('JS')) {
        die('JS() was declared by other libraries, which may cause problems!');
    } else {
        function JS($path, $vars = null)
        {
            $realPath = \Gini\Core::locatePharFile('web/assets/js', "$path.js");
            if (!$realPath) {
                $realPath = \Gini\Core::locatePharFile('raw/assets/js', "$path.js");
            }
            if (!$realPath) {
                return '';
            }
            return \Gini\IoC::construct('\Gini\View\Javascript', $realPath, $vars);
        }
    }

    if (function_exists('HH')) {
        die('HH() was declared by other libraries, which may cause problems!');
    } else {
        function HH($str, $convert_return=FALSE){
            if(is_array($str)){
                $str = implode(' ', $str);
            }
            $str = H($str);
            return $convert_return ? preg_replace('/\n/', '<br/>', $str) : $str;
        }
    }

}
