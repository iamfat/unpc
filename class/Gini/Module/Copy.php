<?php 
namespace Gini\Module;


	class Copy {

		private static $dbName;
		private static $dbAddress;
		private static $dbUser;
		private static $dbPassword;

		// 备份数据库
		/*
		* $fileName 文件名
		* $src      备份路径
		* $tables   [可选]备份表
		*	$dbName  	数据库名
		* return 		false:失败 true:成功
		*/
		static function mysqlDump($fileName,$src,$tables = [],$dbName = null) {
			
			self::dbInfo($dbName);

			$dump = 'mysqldump -u%s -p%s -h%s %s %s > %s/%s';

			if(empty($tables)){
				$sql_arr = explode(',', \Gini\Config::get('template.default')['tables'] );
			}else {
				$sql_arr = $tables;
			}
			
			$tab = '';
    	foreach ($sql_arr as $v) {
    		$tab .= $v . ' ';
    	}
    	$sql = sprintf($dump,self::$dbUser,self::$dbPassword,self::$dbAddress,self::$dbName,$tab,$src,$fileName);

			@exec($sql,$arr,$status);
			return $status == 0 ? true:false;
		}

		// 恢复数据库
		/*
		* $src      恢复数据库路径
		* $dbName   数据库名
		* return 		false:失败 true:成功
		*/
		static function mysqlRecover($src,$dbName = null) {

			self::dbInfo($dbName);

			$recover = 'mysql -u%s -p%s -h%s %s < %s';
			$sql = sprintf($recover,self::$dbUser,self::$dbPassword,self::$dbAddress,self::$dbName,$src);
			@exec($sql,$arr,$status);

			return $status == 0 ? true:false;
		}



		static function dbInfo($name = null){
			$name = $name ?: 'default';
			$opt  = \Gini\Config::get('database.'.$name);
			self::$dbName 	  = explode('=',explode(';',$opt['dsn'])[0])[1];
			self::$dbAddress  = explode('=',explode(';',$opt['dsn'])[1])[1];
			self::$dbUser 	  = $opt['username'];
			self::$dbPassword = $opt['password'];
		}


}




?>