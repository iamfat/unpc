<?php

namespace Gini\Module;

class Help {

    static function pagination(& $objects, $start, $per_page, $url=NULL, $params = [])
    {
        unset($params['st']);

        $totalCount = $objects->totalCount();

        $start = $start - ($start % $per_page);

        if($start > 0) {
            $last = floor($totalCount / $per_page) * $per_page;
            if ($last == $totalCount) $last = max(0, $last - $per_page);
            if ($start > $last) {
                $start = $last;
            }
        }

        $objects = $objects->limit($start, $per_page);

        $pagination = \Model\Widget::factory('pagination', [
                'start' => $start,
                'per_page' => $per_page,
                'total' => $totalCount,
                'url' => $url,
        'params' => $params
            ]);
        return $pagination;
    }

    static function links($links) 
    {
        return \Model\Widget::factory('links', ['items' => $links]);
    }

    static function jsQuote($str, $quote='"') 
    {
        if (is_scalar($str)) {
            if (is_numeric($str)) {
                return $str;
            }
            elseif (is_bool($str)) {
                return $str ? true : false;
            }
            elseif (is_null($str)) {
                return 'null';
            }
            else {
                return $quote.self::escape($str).$quote;
            }
        }
        else {
            return @json_encode($str);
        }
    }

    static function escape($str) 
    {
        return addcslashes($str, "\\\'\"&\n\r<>");
    }

    static function cutHtml($html='')
    {
        $cHtml = strip_tags($html);
        // $b = [ 
        //     "/<script.*>(.*)<\/script>/siU", 
        //     '/on(click|dblclick|mousedown|mouseup|mouseover|mousemove|mouseout|keypress|keydown|keyup)="[^"]*"/i', 
        //     '/on(abort|beforeunload|error|load|move|resize|scroll|stop|unload)="[^"]*"/i', '/on(blur|change|focus|reset|submit)="[^"]*"/i', '/on(bounce|finish|start)="[^"]*"/i', 
        //     '/on(beforecopy|beforecut|beforeeditfocus|beforepaste|beforeupdate|contextmenu|cut)="[^"]*"/i', 
        //     '/on(drag|dragdrop|dragend|dragenter|dragleave|dragover|dragstart|drop|losecapture|paste|select|selectstart)="[^"]*"/i', 
        //     '/on(afterupdate|cellchange|dataavailable|datasetchanged|datasetcomplete|errorupdate|rowenter|rowexit|rowsdelete|rowsinserted)="[^"]*"/i', 
        //     '/on(afterprint|beforeprint|filterchange|help|propertychange|readystatechange)="[^"]*"/i', 
        //     '/javascript\:.*(\;|")/'
        // ]; 
        // $c = ['$1','','','','','','','','','']; 
        return $cHtml; 
    }
}
