<?php

namespace Gini\ORM\Post;

class Tag extends \Gini\ORM\Object
{
	public $tag        = 'object:tag';
	public $post       = 'object:post';

	protected static $db_index = [
		'unique:tag,post'
	];
}
