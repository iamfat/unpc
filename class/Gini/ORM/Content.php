<?php

namespace Gini\ORM;

// 便于后续进行扩展其他不属于标准数据库行列的数据

class Content extends Object
{
	public $name        = 'string:50';
	public $title   	= 'string:50';
	public $description = 'string:*';
    public $weight		= 'int,default:0';
    public $ctime       = 'datetime';
    public $group       = 'string:20';

	protected static $db_index = [
        'name',
        'title',
        'weight',
        'ctime',
        'group'
    ];

    function filePath($path='')
	{
		return APP_PATH.'/'.DATA_DIR.'/content/'.($this->id?:0).'/'.$path;
	}

    public function save()
    {
        if ($this->ctime == '0000-00-00 00:00:00' || !$this->ctime) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }
}