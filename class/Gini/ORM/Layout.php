<?php

namespace Gini\ORM;

class Layout extends Object
{
	public $name        = 'string:50';
	public $index 		= 'int,default:0';
	public $type 		= 'int,default:0';
	public $sizex		= 'int,default:1';
	public $sizey		= 'int,default:1';
	public $col 		= 'int,default:0';
	public $row			= 'int,default:0';
	public $global		= 'int,default:0';

	protected static $db_index = [
		'unique:index',
        'name','type',
        'sizex', 'sizey',
		'col', 'row', 'global'
	];

	const TYPE_NAVBAR = 0;
	const TYPE_DISPLAY = 1;
	const TYPE_EQPIC = 2;
	const TYPE_LOGIN = 3;
	const TYPE_LINK = 4;
	const TYPE_MODULE = 5;
	const TYPE_LOGO = 6;
	const TYPE_TOP_RESERVE_EQ = 7;
	const TYPE_TOP_RESERVE_USER = 8;
	const TYPE_SUMMARY = 9;
	const TYPE_SEARCH = 10;
	const TYPE_TOP_BOX = 11;
	const TYPE_REAL_RECORD = 12;
	const TYPE_NEW_RESERV = 13;
	const TYPE_USER_STATUS = 14;
	const TYPE_LAB_STATUS = 15;

	public static $TYPE = [
		self::TYPE_NAVBAR => '导航菜单',
		self::TYPE_DISPLAY => '展示图片',
		self::TYPE_EQPIC => '仪器图片',
		self::TYPE_LOGIN => '登陆侧框',
		self::TYPE_LINK => '链接底框',
		self::TYPE_MODULE => '系统模块',
		self::TYPE_LOGO => 'logo图',
		self::TYPE_SEARCH => '搜索框',
		self::TYPE_TOP_BOX => '顶部框',
		self::TYPE_TOP_RESERVE_EQ => '仪器预约排行',
		self::TYPE_TOP_RESERVE_USER => '用户使用排行',
		self::TYPE_REAL_RECORD => '仪器实时记录',
		self::TYPE_SUMMARY => '仪器使用情况',
		self::TYPE_NEW_RESERV => '最新预约排行',
		self::TYPE_USER_STATUS => '人员分布情况',
		self::TYPE_LAB_STATUS => '课题测试情况'
	];

	public static $TYPE_VIEW = [
		self::TYPE_NAVBAR => 'navbar',
		self::TYPE_DISPLAY => 'display',
		self::TYPE_EQPIC => 'eqpic',
		self::TYPE_LOGIN => 'login',
		self::TYPE_LINK => 'link',
		self::TYPE_MODULE => 'module',
		self::TYPE_LOGO => 'logo',
		self::TYPE_TOP_RESERVE_EQ => 'top-equipments',
		self::TYPE_TOP_RESERVE_USER => 'top-users',
		self::TYPE_SUMMARY => 'summary',
		self::TYPE_SEARCH => 'search',
		self::TYPE_TOP_BOX => 'top-box',
		self::TYPE_REAL_RECORD => 'real-record',
		self::TYPE_NEW_RESERV => 'new-reserv',
		self::TYPE_USER_STATUS => 'user-status',
		self::TYPE_LAB_STATUS => 'lab-status'
	];

	public function filePath($path='') 
	{
		return APP_PATH.'/'.DATA_DIR.'/layout/'.($this->index?:0).'/'.$path;
	}

    /**
     * @param null $view
     * @param bool $return
     * @param array $vars
     * @return $this|string
     *
     *
     */

	function render($view=NULL, $return = FALSE, $vars=[]){
		if(!$view) {
			$v = self::$TYPE_VIEW[$this->type];
			$view = U("components/{$this->name()}/{$v}");
		}
		$view = ($view instanceof \Gini\View) ? $view : U($view);
		$view->set($vars);
		$view->object = $this;
		if ($return) return (string) $view;
		echo $view;
		return $this;
	}

	function save() {
		if ($this->global) {
			$layout = a('layout', ['global' => 1]);
			while ($layout->id && $layout->id != $this->id) {
				$layout->global = 0;
				$layout->save();
				$layout = a('layout', ['global' => 1]);
			}
		}
        return parent::save();
	}
}
