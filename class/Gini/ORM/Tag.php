<?php

namespace Gini\ORM;

class Tag extends Object
{
	public $name        = 'string:50';
	public $identity	= 'string:50';
	public $category	= 'object:category';
	public $parent		= 'object:tag';
	public $root		= 'object:tag';
	public $weight		= 'int';
	public $type 		= 'int,default:0';

	protected static $db_index = array(
        'unique:name,category',
        'category','parent','root','weight'
        );

	const TYPE_LIST = 0;
	const TYPE_POST = 1;

	public static $TYPE = [
		self::TYPE_LIST => '列表模式',
		self::TYPE_POST => '文章模式'
	];

	function links($mode='list')
	{
		$links = [];
		switch ($mode) {
			case 'index':
				break;
			default:
				$links['edit'] = [
					'url' => 'gini-ajax:AJAX/Unpc/Tag/Edit/'.$this->id,
					'class' => 'icon-edit',
					'title' => ''
				];
				$links['delete'] = [
					'url' => 'gini-ajax:AJAX/Unpc/Tag/Delete/'.$this->id,
					'class' => 'icon-cancel',
					'extra' => 'data-confirm="您确认删除该分类吗?" data-confirm-description="点击确认不会删除相应关联文章，但是会删除该分类信息，请谨慎处理!"',
					'title' => ''
				];
				break;
		}

		return $links;
	}

}
