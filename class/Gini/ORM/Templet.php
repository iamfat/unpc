<?php

namespace Gini\ORM;

class Templet extends Object
{
	public $name		= 'string:50';
	public $path 		= 'string:150';
	// public $parent	= 'object:mould';

	protected static $db_index = [
		'unique:name'
	];

    public function __call($method, $params)
    {
        if ($method == __CLASS__) {
            return;
        }

        if (method_exists('\Gini\Those', $method)) {
            if (!$this->those) {
                $this->those = \Gini\IoC::construct('\Gini\Those', $this->name());
            }
            call_user_func_array(array($this->those, $method), $params);

            return $this;
        }

        return call_user_func_array(array($this->object, $method), $params);
    }
	
}