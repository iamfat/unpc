<?php

namespace Gini\ORM;

class User extends Object
{
    public $name        = 'string:50';
    public $name_abbr   = 'string:100';
    public $gender      = 'bool,null';
    public $username    = 'string:120';
    public $admin       = 'bool,null';
    public $email       = 'string:120';
    public $phone       = 'string:120';
    public $ctime       = 'datetime';
    public $group       = 'array';

    protected static $db_index = array(
        'unique:username',
        'unique:email',
        'name_abbr',
        'ctime',
        );

    public function isAllowedTo($action, $object = null, $when = null, $where = null) {
        if ($object === null) {
            return \Gini\Event::trigger(
                "user.isAllowedTo[$action]",
                $this, $action, null, $when, $where);

        }

        $oname = is_string($object) ? $object : $object->name();
        return \Gini\Event::trigger(
            [
                "user.isAllowedTo[$action].$oname",
                "user.isAllowedTo[$action].*"
            ],
            $this, $action, $object, $when, $where);
    }

    public function icon()
    {
        return new \Model\Icon($this);
    }

    public function save()
    {
        $this->name_abbr = \Model\PinYin::code($this->name);
        if ($this->ctime == '0000-00-00 00:00:00' || !$this->ctime) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

}
