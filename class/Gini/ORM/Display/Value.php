<?php

namespace Gini\ORM\Display;

class Value extends \Gini\ORM\Object
{
	public $display		= 'object:display';
	public $content		= 'string:*';

	protected static $db_index = [
		'unique:display'
	];
}