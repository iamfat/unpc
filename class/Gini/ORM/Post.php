<?php

namespace Gini\ORM;

class Post extends Object
{
	public $title       = 'string:50';
	public $content		= 'string:**';
	public $category	= 'object:category';
	public $status		= 'int,default:0';
	public $author		= 'object:user';
	public $pub_time	= 'datetime';
	public $ctime		= 'datetime';

	protected static $db_index = [
		'category','status','author',
        'title','ctime'
	];

	const STATUS_DRAFT = 0;
	const STATUS_PUBLISH = 1;

	public static $STATUS = [
		self::STATUS_DRAFT => '待发布',
		self::STATUS_PUBLISH => '已发布',
	];

	function filePath($path='')
	{
		return APP_PATH.'/'.DATA_DIR.'/post/'.($this->id?:0).'/'.$path;
	}

	function save()
	{
		if (!$this->ctime || $this->ctime == '0000-00-00 00:00:00') $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
	}

	function links($mode='list')
	{
		$links = [];
		switch ($mode) {
			case 'index':
				break;
			default:
				$links['edit'] = [
					'url' => \Gini\URI::url(strtr('command/post/edit/%id', [
						'%id' => $this->id
					])),
					'class' => 'icon-edit',
					'title' => ''
				];
				$links['delete'] = [
					'url' => '#',
					'class' => 'icon-cancel',
					'extra' => 'data-confirm="您确认删除本条文章吗?" data-confirm-description="点击确认后会删除该文章信息, 请谨慎处理!" data-ajax="gini-ajax:AJAX/Unpc/Post/Delete/'.$this->id.'"',
					'title' => ''
				];
				break;
		}
		return $links;
	}

}
