<?php

namespace Gini\ORM;

class Category extends Object
{
	public $name        = 'string:50';
	public $identity	= 'string:50';
	public $type 		= 'int,default:0';
	public $parent		= 'object:category';
	public $weight		= 'int,default:0';

	protected static $db_index = [
		'unique:identity',
        'name',
        'parent',
        'weight',
		'type'
	];

	const TYPE_POST = 0;
	const TYPE_FILE = 1;

	public static $TYPE = [
		self::TYPE_POST => '文章',
		self::TYPE_FILE => '文件'
	];

	function url($index='setting')
	{
		$identity = $this->parent->id ? $this->parent->identity : $this->identity;
		return \Gini\URI::url(strtr('/command/module/%identity/%index', [
			'%identity' => $identity,
			'%index' => $index
		]));
	}
}
