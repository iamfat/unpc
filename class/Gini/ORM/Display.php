<?php

namespace Gini\ORM;

class Display extends Object
{
	public $identity	= 'string:50';
	public $type 		= 'int,default:1';

	protected static $db_index = [
		'unique:identity',
		'type'
	];

	const TYPE_LIST = 1;
	const TYPE_POST = 2;

	public static $TYPE = [
		self::TYPE_LIST => '列表',
		self::TYPE_POST => '文章',
	];

	function render($view=NULL, $return = FALSE, $vars=[]){
		if(!$view) $view = U('components/'.$this->name());
		$view = ($view instanceof \Gini\View)? $view : U($view);
		$view->set($vars);
		$view->object = $this;
		if ($return) return (string) $view;
		echo $view;
		return $this;
	}
}
