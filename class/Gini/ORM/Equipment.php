<?php

namespace Gini\ORM;

class Equipment extends Object
{
    public $rid   	        = 'int,not null';
    public $chargeRule      = 'string:*';       // 参考收费标准
    public $openCalendar    = 'string:*';       // 开放机时安排
    public $application     = 'string:*';       // 测试研究领域
    public $frontShow		= 'int,default:0';  // 是否在仪器列表显示
    public $description     = 'string:*';       // 备注信息
	public $ctime		    = 'datetime';

	protected static $db_index = [
        'unique:rid', 'ctime', 'frontShow'
	];

	function filePath($path='')
	{
		return APP_PATH.'/'.DATA_DIR.'/equipment/'.($this->id?:0).'/'.$path;
	}

	function save()
	{
		if (!$this->ctime || $this->ctime == '0000-00-00 00:00:00') $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
	}

}