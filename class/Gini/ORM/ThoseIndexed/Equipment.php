<?php

namespace Gini\ORM\ThoseIndexed;

class Equipment extends \Gini\ORM\Object
{
	private $_token;
	private $_total;
	private $_criteria;

	/**
	* 获取默认指定API路径的RPC对象
	*
	* @return new RPC
	**/
    protected static $_RPC = [];
    protected static function getRPC()
    {
        if (!isset(self::$_RPC['equipment'])) {
        	$sys = (array)Hub('config.design.system');
            try {
                $rpc = \Gini\IoC::construct('\Gini\RPC', $sys['url']);
                self::$_RPC['equipment'] = $rpc;
            } catch (RPC\Exception $e) {
            }
        }

        return self::$_RPC['equipment'];
    }

    public function filter(array $criteria)
    {
        $this->_criteria = $criteria;
    }

	public function limit($start = 0, $step = 10)
	{
		$rpc = self::getRPC();

		try {
			if (!$this->_token) {
				$r = $rpc->equipment->searchEquipments($this->_criteria);
				$this->_token = $r['token'];
				$this->_total = $r['total'];
			}
			
            $equipments = $rpc->equipment->getEquipments($this->_token, $start, $step);

            return $equipments;

		} catch (\Gini\RPC\Exception $e) {
			return [];
		}
	}

	public function totalCount()
	{
		if (!$this->_token) {
			$rpc = self::getRPC();
			if (!$rpc) return 0;

			$r = $rpc->equipment->searchEquipments($this->_criteria);
			$this->_token = $r['token'];
			$this->_total = $r['total'];
		}
		return (int)$this->_total;
	}

}