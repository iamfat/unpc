<?php

namespace Gini\Controller\CGI;

class Content extends Layout\Unpc {
    
    function __index($id = 0) 
    {
        $content = a('content', $id);
        if (!$content->id) $this->redirect('error/404');
       
        $this->view->body = U('content/view', [
            'content' => $content
        ]);
    }
    
}
