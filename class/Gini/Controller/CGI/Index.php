<?php

namespace Gini\Controller\CGI;

class Index extends Layout\Unpc {
    
    function __index() {

        setcookie('#LOGOUT_REFERER', URL('/'), time()+3600*12, '/', $_SERVER['SERVER_NAME']);
    	$layouts = those('layout')->orderBy('row')->orderBy('col');
        $this->view->body = U('index', [
        		'layouts' => $layouts
        	]);
    }

    function actionLogOut() {
        \Gini\Auth::logout();
        $this->redirect('/entry');
    }

}
