<?php

namespace Gini\Controller\CGI\Command;
/**
 * Class Post
 * @package Gini\Controller\CGI\Command
 * 文章上传类
 */
class Post extends \Gini\Controller\CGI\Layout\Admin
{
    function __index($id=0)
    {
        // TODO
    }

    function actionAdd($identity='')
    {
        //是否在特定种类中
        $category = a('category', ['identity' => $identity]);
        if (!$identity || !$category->id) $this->redirect('unpc/error/404');
        if ($category->parent->id) $this->redirect('unpc/error/401');
        //获取登陆信息
        $me = _G('ME');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //获取post参数 并校验
			$form = $this->form('post');
            $validator = new \Gini\CGI\Validator;
            try {
                $p = a('post');
                $validator
                    ->validate('title', H($form['title']), T('标题不能为空!'))                    
                    ->done();
                $post = a('post');
                $post->title = $form['title'];
                $post->content = $form['content'];
                $post->author = $me;
                $post->category = $category;
                if ($form['submit'] == 'publish') {
                    $post->status = \Gini\ORM\Post::STATUS_PUBLISH;
                    $post->pub_time = date('Y-m-d H:i:s');
                }
                //上面是构造数据库信息
                if ( $post->save() ) {
                    //保存之后开始完善其他表的数据。
                    //下面是 维护 post_tag表
                    foreach((array)$form['tag'] as $k) {
                        $t = a('tag', $k);
                        if ($t->id) {
                            $connect = a('post/tag');
                            $connect->tag = $t;
                            $connect->post = $post;
                            $connect->save();
                        }
                    }
                    //下面是如果分类为文件格式则执行if内部
                    if ($post->category->type == \Gini\ORM\Category::TYPE_FILE) {
                        //filePath:APP_PATH.'/'.data.'/post/'.($this->id?:0).'/';
                        if (is_dir($p->filePath())) {
                            //如果存在post/0目录 则移动到post/id 目录 在文件模式下。
                            \Gini\File::copy($p->filePath(), $post->filePath());
                            \Gini\File::removeDir($p->filePath());
                            //遍历目录下所有文件
                            \Gini\File::eachFilesIn($post->filePath(), function($file) use($post) {
                                $post->fullPath = $post->filePath($file);
                                $post->fileName = $file;
                                $post->fileExtension = pathinfo($file, PATHINFO_EXTENSION);
                                $post->save();
                            });
                        }
                    }

                    $this->redirect(strtr('/command/post/edit/%id', [
                        '%id' => $post->id
                    ]));
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $this->view->body = U('admin/index')
                ->set('content', U('admin/post/add', [
                    'form' => $form,
                    'category' => $category,
                    'tags' => those('tag')->whose('category')->is($category)
                                ->orderBy('weight')
                ]));
    }

    function actionEdit($id=0)
    {
        $post = a('post', $id);
        if (!$post->id) $this->redirect('command/error/404');
        $me = _G('ME');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $form = $this->form('post');
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('title', H($form['title']), T('标题不能为空!'))
                    ->done();

                $post->title = $form['title'];
                $post->content = $form['content'];
                $post->author = $me;
                if ($form['submit'] == 'publish') {
                    $post->status = \Gini\ORM\Post::STATUS_PUBLISH;
                    $post->pub_time = date('Y-m-d H:i:s');
                }
                else {
                    $post->status = \Gini\ORM\Post::STATUS_DRAFT;
                    $post->pub_time = '0000-00-00 00:00:00';
                }
                if ( $post->save() ) {
                    foreach(those('post/tag')->whose('post')->is($post) as $pt) {
                        $pt->delete();
                    }
                    foreach((array)$form['tag'] as $k) {
                        $t = a('tag', $k);
                        if ($t->id) {
                            $connect = a('post/tag');
                            $connect->tag = $t;
                            $connect->post = $post;
                            $connect->save();
                        }
                    }
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $ctags = [];
        $ptags = those('post/tag')->whose('post')->is($post);
        foreach ($ptags as $c) {
            $ctags[] = $c->tag->id;
        }

        $this->view->body = U('admin/index')
                ->set('content', U('admin/post/edit', [
                    'form' => $form,
                    'post' => $post,
                    'tags' => those('tag')->whose('category')->is($post->category)
                                ->orderBy('weight'),
                    'ctags' => $ctags

                ]));
    }
}
