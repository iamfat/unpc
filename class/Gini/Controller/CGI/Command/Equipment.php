<?php

namespace Gini\Controller\CGI\Command;

class Equipment extends \Gini\Controller\CGI\Layout\Admin
{
    function __index($id=0)
    {
        if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();
        $this->view->body = V('admin/index')
                ->set('content', V('admin/equipment/list', [
                    'form' => $form
                ]));
    }
}