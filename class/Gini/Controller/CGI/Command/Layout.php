<?php

namespace Gini\Controller\CGI\Command;

class Layout extends \Gini\Controller\CGI\Layout\Admin
{
    function __index($id=0)
    {
        // TODO
    }

    function actionSetView($index=0)
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();
        $object = a('layout')->whose('index')->is($index);
        if (!$object->id) {
            $this->redirect('error/404');
        }
        $this->view->body = U('admin/index')
                            ->set('content', U('components/layout/set/one', [
                                'form' => $form,
                                'object' => $object,
                            ]));
    }
}
