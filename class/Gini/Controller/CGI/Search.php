<?php

namespace Gini\Controller\CGI;

class Search extends Layout\Unpc {
    
    function __index() 
    {
        $form = $this->form();
        error_log("token:{$_SESSION['token']}");
        $x = md5($_SESSION['token']);
        error_log("token_md5:{$x}");
        if (H($form['token']) != md5($_SESSION['token'])) {
            $this->redirect('error/401');
        }

        if ($form['search_type'] == 'equipment') {
            $this->redirect(URL('category/equipment', [
                'keywords' => H($form['keywords']),
                'token' => H($form['token'])
            ]));
            exit();
        }
        elseif ($form['search_type'] != 'all') {
            $this->redirect(URL('error/404'));
        }
 
        $vars = [
            'keywords' => H($form['keywords'])
        ];

        $posts = those('post')
                    ->whose('status')->is(\Gini\ORM\Post::STATUS_PUBLISH)
                    ->whose('title')->contains($vars['keywords'])
                    ->orWhose('content')->contains($vars['keywords']);

        $vars['posts'] = $posts;

        $this->view->body = U('search', $vars);
    }
    
}
