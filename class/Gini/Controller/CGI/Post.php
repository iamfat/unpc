<?php

namespace Gini\Controller\CGI;

class Post extends Layout\Unpc {
    
    function __index($id = 0) 
    {
        $post = a('post', $id);
        if (!$post->id) $this->redirect('error/404');
        $category = $post->category;
        $display = a('display', ['identity' => $category->identity]);
        if ($display->clickAction == 'download') {
            $fullpath = $post->fullPath;
            $filename = $post->fileName;
            // $encoded_filename = urlencode($filename);
            // $encoded_filename = str_replace("+", "%20", $encoded_filename);
            $mime_type = \Gini\File::mimeType($fullpath);

            header("Content-Type: $mime_type");
            header('Accept-Ranges: bytes');
            header('Accept-Length:'.filesize($fullpath));
            header("Content-Disposition: attachment; filename=\"$filename\"");
            ob_clean();
            echo file_get_contents($fullpath);
            exit;
        }
        else {
            $this->view->body = U('post/view', [
                'post' => $post, 
                'display' => $display, 
                'category' => $category
            ]);
        }

    }
    
}
