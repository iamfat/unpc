<?php

namespace Gini\Controller\CGI\Hnucm;

class Command extends \Gini\Controller\CGI\Layout\Admin
{
    public function actionInstrument()
    {
        $form = $this->form();
        
        $contents = those('content')->whose('group')->is('instrument');

        $this->view->body = U('admin/index')
                ->set('content', U('admin/instrument', [
                    'form' => $form,
                    'contents' => $contents
                ]));
    }

    public function actionPlatform()
    {
        $form = $this->form();
        
        $contents = those('content')->whose('group')->is('platform');

        $this->view->body = U('admin/index')
                ->set('content', U('admin/platform', [
                    'form' => $form,
                    'contents' => $contents
                ]));
    }
}
