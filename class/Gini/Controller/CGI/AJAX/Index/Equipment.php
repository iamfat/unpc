<?php

namespace Gini\Controller\CGI\AJAX\Index;

class Equipment extends \Gini\Controller\CGI {

    public function actionGetDisplayEquipments()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $sysSetting = (array)Hub('config.design.system');
        $form = $this->form('post');
        $layout = a('layout', (int)$form['id']);
        $selector = [];
        if ((int)$form['cat']) {
            $selector['cat'] = (int)$form['cat'];
        }
        if ((int)$form['group']) {
            $selector['group'] = (int)$form['group'];
        }
        // 仪器是否在前台显示
        $showEqIds = those('equipment')->whose('frontShow')->is(1);
        $selector['ids'] = $showEqIds->totalCount() ? $showEqIds->get('rid') : '-1';

        try {
            $apiUrl = $sysSetting['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $result = $rpc->equipment->searchEquipments($selector);
            $apiSelector = $result['token'];
            $count = (int)$layout->count ? : (\Gini\Config::get('system.index_equipment_page') ?: 5);
            $equipments = $rpc->equipment->getEquipments($apiSelector, 0, $count);
        } catch(\Exception $e){
            $equipments = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/index-list', [
                'equipments' => $equipments,
                'layout' => $layout
            ]));
    }

	public function actionGetRoot() {

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }

        $setting = (array)Hub('Module.equipment.setting');
        $sys = (array)Hub('config.design.system');
        $root = $setting['root'] == 'tag' ? 'cat' : 'group';
        $selectTags = (array)$setting['selectTags'];
        $form = $this->form();
        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $tags = [];
            if ($root == 'cat') {
                if (count($selectTags)) {
                    foreach ($selectTags as $name) {
                        $tags += $rpc->equipment->getEquipmentTags(['tag_name' => $name]);
                    }
                }
                else {
                    $tags = $rpc->equipment->getEquipmentTags();
                }
            }
            else {
                if (count($selectTags)) {
                    foreach ($selectTags as $name) {
                        $tags += $rpc->equipment->getEquipmentGroups(['group_name' => $name]);
                    }
                }
                else {
                    $tags = $rpc->equipment->getEquipmentGroups();
                }
                foreach ($tags as $id => $tag) {
                    $children = $tags[$id]['children'];
                    $tags[$id] = ['name' => $tag['name']];
                    foreach ($children as $cid => $child) {
                        $tags[$cid]  = ['name' => $child];
                    }
                }
            }
        }
        catch (\Exception $e) {
            $tags = [];
        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/root', [
                'data' => $tags,
                'baseUrl' => H($form['u']),
                'key' => H($form['key']),
                'id' => (int)$form['id']
            ]));
    }

    public function actionGetEquipments($start=0)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $setting = (array)Hub('Module.equipment.setting');
        $sys = (array)Hub('config.design.system');
        $totalCount = 0;

        try {
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $step = 20;
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $form = $this->form('post');
            $ajax_form = (array)$form['ajax_form'];
            $vars = ['searchtext' => H($ajax_form['keywords'])];
            $name = $form['name'] ? : ($setting['root'] == 'tag' ? 'cat' : 'group');
            if (isset($ajax_form['id'])) {
                $vars[$name] = (int)$ajax_form['id'];
            }
            if ((int)$ajax_form['cat']) {
                $vars['cat'] = (int)$ajax_form['cat'];
            }
            if ((int)$ajax_form['group']) {
                $vars['group'] = (int)$ajax_form['group'];
            }
            // 仪器是否在前台显示
            $showEqIds = those('equipment')->whose('frontShow')->is(1);
            $vars['ids'] = $showEqIds->totalCount() ? $showEqIds->get('rid') : '-1';
            $result = $rpc->equipment->searchEquipments($vars);
            $apiSelector = $result['token'];
            $totalCount = $result['total'];
            if (\Gini\Config::get('theme.default') == 'shtech') {
                // 上科大定制-仪器目录排序
                if ((int)$ajax_form['id']) {
                    $equipments = $rpc->equipment->getEquipments($apiSelector, $start, $step);
                }
                else {
                    $equipments = $rpc->hot_equipment->get_equipments_hot($vars['searchtext'], $start, $step);
                }
            }
            else {
                $equipments = $rpc->equipment->getEquipments($apiSelector, $start, $step);
            }
            
        }
        catch (\Exception $e) {
            $equipments = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/equipments-ajax', [
                'equipments' => $equipments,
                'next_start' => $start + $step,
                'totalCount' => $totalCount
            ]));
    }

    public function actionGetTopEquipments()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $sys = (array)Hub('config.design.system');
        $form = $this->form('post');
        $layout = a('layout', (int)$form['id']);
        try {
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $number = (int)$layout->count;
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $equipments = $rpc->eq_reserv->getTopEquipments($number, $layout->year);
        }
        catch (\Exception $e) {
            $equipments = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/top-equipments', [
                'equipments' => $equipments
            ]));
    }

    public function actionGetTopUsers()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $sys = (array)Hub('config.design.system');
        $form = $this->form('post');
        $layout = a('layout', (int)$form['id']);
        try {
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $number = (int)$layout->count;
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $users = $rpc->eq_reserv->getTopUsers($number, $layout->year);
        }
        catch (\Exception $e) {
            $users = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/top-users', [
                'users' => $users
            ]));
    }

    public function actionGetSummary()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $sys = (array)Hub('config.design.system');
        $form = $this->form('post');
        try {
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $summary = $rpc->equipment->getSummaryInfo();
        }
        catch (\Exception $e) {
            $summary = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/summary', [
                'summary' => (array)$summary
            ]));
    }

    public function actionGetSelectTags()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $form = $this->form();

        $setting = (array)Hub('Module.equipment.setting');
        $sys = (array)Hub('config.design.system');
        $root = $form['root'] == 'tag' ? 'cat' : 'group';
        $name = $setting['root_name'];

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);

            if ($root == 'cat') {
                $tags = $rpc->equipment->getEquipmentTags();
            }
            else {
                $tags = $rpc->equipment->getEquipmentGroups();
            }
        }
        catch (\Exception $e) {
            $tags = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/html', [
                'html' => U('equipment/select-tags', [
                        'tags' => (array)$tags
                    ]),
                'mode' => 'replace',
                'selector' => H($form['selector'])
            ]));

    }

    public function actionGetRecords()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $form = $this->form();
        $sys = (array)Hub('config.design.system');
        $records = [];
        $start = $form['start'] ?: 0;
        $step = $form['step'] ?: 20;

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $records = $rpc->equipment->getNewUsers($start, $step);
        }
        catch (\Exception $e) {
            $records = [];
        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/real-records', [
            'records' => (array)$records
        ]));
    }

    public function actionGetNewUsers()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $sys = (array)Hub('config.design.system');
        $form = $this->form('post');
        $number = $form['number'] ?: 10;
        $time = $form['time'] ?: time();
        try {
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $users = $rpc->eq_reserv->getNewUsers($number, $time);
        }
        catch (\Exception $e) {
            $users = [];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('equipment/new-reserv', [
                'users' => $users
            ]));
    }

    // 可自行传入参数和试图来进行tag的获取
    public function actionGetRootByForm() {
        $sys = (array)Hub('config.design.system');
        $form = $this->form();
        $name = H($form['name']);
        $res_type = H($form['res_type']);

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $tags = [];
            if ($form['root'] == 'cat') {
                $tags = $rpc->equipment->getEquipmentTags(['tag_name' => $name, 'order_by' => 'weight']);
            }
            else {
                $tags = $rpc->equipment->getEquipmentGroups(['group_name' => $name, 'order_by' => 'weight']);
                foreach ($tags as $id => $tag) {
                    $children = $tags[$id]['children'];
                    $tags[$id] = ['name' => $tag['name']];
                    if (!(bool)$form['no_children']) {
                        foreach ((array)$children as $cid => $child) {
                            $tags[$cid]  = ['name' => $child['name']];
                        }
                    }
                }
            }
        }
        catch (\Exception $e) {
            $tags = [];
        }

        if ($res_type == 'json') {
            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', J($tags));
        }
        else {
            $view = H($form['view']) ?: 'equipment/root';
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
                'data' => $tags
            ]));
        }
    }

    public function actionGetSummaryInfo()
    {
        $sys = (array)Hub('config.design.system');
        $form = $this->form();
        $res_type = H($form['res_type']);

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $info = [];
            $info = $rpc->equipment->getSummaryInfo();
        }
        catch (\Exception $e) {
            $info = [];
        }

        if ($res_type == 'json') {
            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', J($info));
        }
        else {
            $view = H($form['view']) ?: 'equipment/summary-info';
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
                'data' => $info
            ]));
        }
    }

    public function actionLabStatus()
    {
        $sys = (array)Hub('config.design.system');
        $form = $this->form();
        $res_type = H($form['res_type']);

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $info = [];
            $id = \Gini\Config::get('equipment.client_id');
            $secret = \Gini\Config::get('equipment.client_secret');
            if ($rpc->Summarize->authorize($id, $secret)) {
                    $info = $rpc->Summarize->labStatus();
            }
        }
        catch (\Exception $e) {
            $info = [];
        }

        $info = $info + ['project' => 0, 'lab' => 0, 'test' => 0];

        if ($res_type == 'json') {
            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', J($info));
        }
        else {
            $view = H($form['view']) ?: 'equipment/lab-status';
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
                'data' => $info
            ]));
        }
    }

    public function actionUserStatus()
    {
        $sys = (array)Hub('config.design.system');
        $form = $this->form();
        $res_type = H($form['res_type']);

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
            $info = [];
            $id = \Gini\Config::get('equipment.client_id');
            $secret = \Gini\Config::get('equipment.client_secret');
            if ($rpc->Summarize->authorize($id, $secret)) {
                $info = $rpc->Summarize->userStatus();
            }
        }
        catch (\Exception $e) {
            $info = [];
        }

        $info = $info + ['total' => 0, 'outer' => 0, 'inner' => 0, 'incharge' => 0];

        if ($res_type == 'json') {
            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', J($info));
        }
        else {
            $view = H($form['view']) ?: 'equipment/user-status';
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
                'data' => $info
            ]));
        }
    }

}