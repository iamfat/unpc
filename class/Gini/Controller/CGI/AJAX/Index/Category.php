<?php

namespace Gini\Controller\CGI\AJAX\Index;

class Category extends \Gini\Controller\CGI 
{

    public function actionGetPosts($start=0,$step=null)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('error/401');
            return false;
        }
        $form = $this->form('post');
        $ajax_form = $form['ajax_form'];
        $identity = H($ajax_form['m']);
        $category = a('category', ['identity' => $identity]);
        if(!isset($step))
        {
            $step = \Gini\Config::get('system.pageCount') ?: 20;
        }
        if($ajax_form['per_page']>0)
        {
            $step = $ajax_form['per_page'];
        }
        $db = a('post')->db();
        $where = [];
        $params = [];

        $SQL = "SELECT P.* FROM post AS P ";

        $where[] = " P.category_id = :category_id ";
        $params[':category_id'] = (int)$category->id;

        $where[] = " P.status = :status ";
        $params[':status'] = \Gini\ORM\Post::STATUS_PUBLISH;

        if ($ajax_form['tag']) {
            $tag = a('tag')->whose('identity')->is(H($ajax_form['tag']));
            $SQL .= "JOIN post_tag AS PT ON PT.post_id = P.id ";

            $where[] = " PT.tag_id = :tag_id ";
            $params[':tag_id'] = (int)$tag->id;
        }

        if (count($where)) {
            $SQL .= 'WHERE ' . join(' AND ', $where);
        }

        $display = a('display', ['identity' => $identity]);
        if ($display->listSort == 'initial') {
            $SQL .= " ORDER BY P.title ";
        }
        else {
            $SQL .= " ORDER BY P.pub_time ";
        }
        if ($display->listOrder =='asc')
        {
            $SQL .= " asc ";
        }
        else {
            $SQL .= " desc ";
        }
        /*
         * 获取start的值
         */
        if($ajax_form['st'])
        {
            $start=$ajax_form['st'];
        }
        //--------
        $SQL .= sprintf(" LIMIT %d, %d", $start, $step);
        $posts = $db->query($SQL, null, $params)->rows();
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('category/posts-ajax', [
                'posts' => $posts,
                'd' => a('display', ['identity' => $category->identity]),
                'next_start' => $start + $step
            ]));
    }

}
