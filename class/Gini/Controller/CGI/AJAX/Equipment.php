<?php

namespace Gini\Controller\CGI\AJAX;

class Equipment extends \Gini\Controller\CGI
{
    public function actionGetEquipments($start = 0, $step = 10)
    {
        $form = $this->form('post');
        $ajax_form = (array) $form['ajax_form'];
        $vars = ['searchtext' => H($ajax_form['keywords'])];
        $name = $form['name'] ?: 'cat';
        if (isset($ajax_form['id'])) {
            $vars[$name] = (int) $ajax_form['id'];
        }

        $those = thoseIndexed('equipment', $vars);

        $view = H($form['box_view']) ?: 'equipment/equipments-ajax';

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
            'equipments' => $those->limit((int) $start, (int) $step),
            'totalCount' => $those->totalCount(),
            'next_start' => $start + $step,
            'form' => $form,
        ]));
    }

    public function actionGetRoot()
    {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            $this->redirect('error/401');
            return false;
        }
        $form = $this->form();
        $sys = (array) Hub('config.design.system');
        $root = $form['root'] ?: 'cat';

        if (count((array)Hub("equipment.{$root}.tags")) > 0 && Hub("equipment.{$root}.expiration") >= time()) {
            $tags = (array)Hub("equipment.{$root}.tags");
        } else {
            try {
                $rpc = \Gini\Ioc::construct('\Gini\RPC', $sys['url']);
                $tags = [];
                if ('cat' == $root) {
                    $tags = $rpc->equipment->getEquipmentTags();
                } else {
                    $tags = $rpc->equipment->getEquipmentGroups();
                }
            } catch (\Exception $e) {
                $tags = [];
            }
            Hub("equipment.{$root}.expiration", time() + 86400);
            Hub("equipment.{$root}.tags", $tags);
        }

        $view = $form['box_view'] ?: 'equipment/root';

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view, [
                'data' => $tags,
                'baseUrl' => H($form['u']),
                'key' => H($form['key']),
                'id' => (int) $form['id'],
            ]));
    }
}
