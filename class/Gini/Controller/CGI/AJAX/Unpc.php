<?php

namespace Gini\Controller\CGI\AJAX;

class Unpc extends \Gini\Controller\CGI {

	function actionUsers($start=0) {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $per_page = 25;

        $form = $this->form()['ajax_form'];
        $db = a('user')->db();
        $sql = "SELECT * FROM user where username <> 'genee|database' ";

        $totalCount = $db->query($sql, null, [])->Count();

        $sql .= sprintf(" LIMIT %d, %d", $start, $per_page);

        $users = $db->query($sql, null, [])->rows();

        $next_start = $totalCount >= ($start + $per_page) ? ($start + $per_page) : -1;

        $vars = array(
            'users' => $users,
            'form' => $form,
            'next_start' => $next_start,
        );

        $view = U('admin/user/users-ajax', $vars);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    function actionSwitch($id=0)
    {
    	$me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $user = a('user', $id);
        if (!$user->id) {
            $this->redirect('error/404');
        }

        $form = $this->form();

        $user->admin = (bool) $form['admin'];
        $user->save();
    }

    function actionAddUser()
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $username = \Gini\Auth::makeUserName(H($form['username']), H($form['backend']));

                $exist_user = a('user')
                            ->whose('username')
                            ->is($username);

                $validator
                    ->validate('name', $form['name'], T('姓名不能为空!'))
                    ->validate('username', $form['username'], T('登录名不能为空!'))
                    ->validate('username', !$exist_user->id, T('账号在系统中已存在!'))
                    ->validate('password', $form['password'], T('密码不能为空!'))
                    ->done();

                $user = a('user');
                $user->name = H($form['name']);
                $user->username = $username;
                $user->email = H($form['email']);
                $user->phone = H($form['phone']);
                $user->save();

				$auth = \Gini\IoC::construct('\Gini\Auth', $username);
        		$auth->create(H($form['password'] ?: '123456'));

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }

        $view = U('admin/user/add-user-modal', ['form' => $form]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    function actionEditUser($id=0)
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $user = a('user', $id);
        if (!$user->id) {
            $this->redirect('error/404');
        }

        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $validator = new \Gini\CGI\Validator;

            try {
                $validator
                    ->validate('password', $form['password'], T('密码不能为空!'))
                    ->done();

                $username = $user->username;

                $auth = \Gini\IoC::construct('\Gini\Auth', $username);
                $auth->changePassword(H($form['password'] ?: '123456'));

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        $view = U('admin/user/edit-user-modal', ['form' => $form, 'user' => $user]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);

    }

    function actionDeleteUser($id=0)
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $user = a('user', $id);
        if ($user->username == 'genee|database') {
            $this->redirect('error/401');
        }
        if (!$user->id) {
            $this->redirect('error/404');
        }

        //remove this user
        $user->delete();

        $view = U('admin/user/delete-user-success', ['user' => $user]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);

    }

    function actionEquipments($start=0) {
        $me = _G('ME');

        $per_page = 15;

        $form = $this->form()['ajax_form'];
        $setting = (array)Hub('config.design.system');

        try {
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $setting[url]);

            $result = $rpc->equipment->searchEquipments([]);

            $totalCount = $result[total];

            $equipments = $rpc->equipment->getEequipments($result[token], $start, $per_page);

            $next_start = $totalCount >= ($start + $per_page) ? ($start + $per_page) : -1;

            $vars = array(
                'equipments' => $equipments,
                'form' => $form,
                'next_start' => $next_start,
            );

            $view = U('admin/equipment/equipments-ajax', $vars);
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
        }
        catch(\Gini\RPC\Exception $e) {
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/equipment/error-ajax', [
                    'error' => $e->getMessage(),
                    'code' => $e->getCode()
                ]));
        }
    }

    public function actionLogin() {
        $form = $this->form();
        $start = (int)$form['start'];
        $end = (int)$form['end'];
        $_SESSION['sliderVerify'] = TRUE;
        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', null);
    }

}
