<?php

namespace Gini\Controller\CGI\AJAX\Hnucm;

class Message extends \Gini\Controller\CGI {

	public function actionSend() {
        try {
            $now = time();

            if (!$_SESSION['SEND_MAIL_EXPIRED_TIME'] ||  $now >= $_SESSION['SEND_MAIL_EXPIRED_TIME']) {
                $_SESSION['SEND_MAIL_EXPIRED_TIME'] = time() + 60 * 10;
                $form = $this->form('post');
                $body = sprintf("老师, 您好: \n\n%s (%s) 提交了大仪平台意见反馈, 具体内容如下: \n\n %s", H($form['name']), H($form['phone']), H($form['body']));
                $mail = new \Gini\Mail;
                $mail
                    ->to('zhaoshengbo@dlmu.edu.cn', '设备处')
                    ->subject('大仪平台意见反馈')
                    ->body($body)
                    ->send();
                $msg = T('您的消息已经发送成功!');
                $type = 'TYPE_OK';
            }
            else {
                throw new \Exception("提交过于频繁, 请您十分钟之后重试!");
            }
        } catch(\Exception $e){
            $msg = $e->getMessage();
            $type = 'TYPE_ERROR';
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/message', [
            'value' => $msg,
            'type' => $type
        ]));
    }

}
