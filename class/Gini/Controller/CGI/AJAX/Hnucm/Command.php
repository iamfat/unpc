<?php

namespace Gini\Controller\CGI\AJAX\Hnucm;

class Command extends \Gini\Controller\CGI {

    public function actionAddInstrument()
    {
        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('title', $form['title'], T('科研设施不能为空!'))
                    ->done();

                $con = a('content');
                $con->group = 'instrument';
                $con->title = H($form['title']);
                $con->name = H($form['name']);
                $con->description = $form['description'];
                $con->link_url = H($form['url']);
                $con->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/add-instrument', [
                'form' => $form
            ]));
    }

    public function actionEditInstrument($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('title', $form['title'], T('科研设施不能为空!'))
                    ->done();
                    
                $content->title = H($form['title']);
                $content->name = H($form['name']);
                $content->description = $form['description'];
                $content->link_url = H($form['url']);
                $content->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/edit-instrument', [
                'form' => $form,
                'content' => $content
            ]));
    }

    public function actionDeleteInstrument($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');
        
        $content->delete();

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
    }

    public function actionEditInstrumentImg($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/edit-instrument-image', [
            'form' => $form,
            'content' => $content
        ]));
    }

    public function actionUpload($id=0)
    {
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');
        $form = $this->form('files');
        $file = $form['file'];
        if ($file) {
            $fileName = $file['name'];
            $fullPath = $content->filePath($fileName);
            \Gini\File::ensureDir($content->filePath());
            move_uploaded_file($file['tmp_name'], $fullPath);
            if (is_file($fullPath)) {
                if ($content->id) {
                    $content->fullPath = $fullPath;
                    $content->fileName = $fileName;
                    $content->fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
                    $content->save();
                }

                return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                    'ok' => H(T('文件上传成功!')),
                    'file' => $fullPath
                ]);
            }
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
            'error' => H(T('文件上传失败!')),
            'file' => $file['name']
        ]);
    }

    public function actionRemoveFile($id=0)
    {  
        $form = $this->form('post');
        $name = H($form['name']);
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');

        $fullPath = $content->filePath($name);
        if (file_exists($fullPath)) {
            if ($content->id) {
                $content->fullPath = '';
                $content->fileName = '';
                $content->fileExtension = '';
                $content->save();
            }
            \Gini\File::delete($fullPath);
        }
    }

    public function actionAddPlatform()
    {
        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('title', $form['title'], T('平台名称不能为空!'))
                    ->done();

                $con = a('content');
                $con->group = 'platform';
                $con->title = H($form['title']);
                $con->name = H($form['name']);
                $con->description = $form['description'];
                $con->link_url = H($form['url']);
                $con->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/add-platform', [
                'form' => $form
            ]));
    }

    public function actionEditPlatform($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('title', $form['title'], T('平台名称不能为空!'))
                    ->done();
                    
                $content->title = H($form['title']);
                $content->name = H($form['name']);
                $content->description = $form['description'];
                $content->link_url = H($form['url']);
                $content->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/edit-platform', [
                'form' => $form,
                'content' => $content
            ]));
    }

    public function actionDeletePlatform($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');
        
        $content->delete();

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
    }

    public function actionEditPlatformImg($id = 0)
    {
        $form = $this->form();
        $content = a('content', $id);
        if (!$content->id) $this->redirect('/error/404');

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/edit-instrument-image', [
            'form' => $form,
            'content' => $content
        ]));
    }

}
