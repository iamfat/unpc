<?php
namespace Gini\Controller\CGI\AJAX;

class User extends \Gini\Controller\CGI {

	function actionCurrent() {
        $form = $this->form('get');

        //把传递过来的base64图片保存本地
        preg_match_all('/src="(.*)?"/',$form['img'],$imgData);
        $iconName = $defaultIconName = '374f29ad3d1c3e74ec4c99dbd7f63057.png';
        if(isset($imgData[1][0])){
            $iconName = md5($imgData[1][0]).'.png';
            if(!@file_exists('web/assets/icon/'.$iconName)){
                @file_put_contents('web/assets/icon/'.$iconName, base64_decode(explode(',',$imgData[1][0])[1]));
            }
            $iconName = getimagesize('web/assets/icon/'.$iconName) ? $iconName : $defaultIconName;
        }
        $view = U('components/layout/login/current', [
            'name' => H($form['name']),
            'img' => '<img class="icon icon_user" width="64px" height="64px" src="'.URL("/assets/icon/$iconName").'"/>'
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }
}
