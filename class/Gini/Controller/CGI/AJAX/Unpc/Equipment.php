<?php

namespace Gini\Controller\CGI\AJAX\Unpc;

class Equipment extends \Gini\Controller\CGI
{
    public function actionList($start = 0)
    {
        $form = $this->form()['ajax_form'];

        $module = 'equipment';
        class_exists('\Gini\ThoseIndexed');
        $vars['searchtext'] = H($form['keywords']);
        $vars['ref_no'] = H($form['ref_no']);

        $per_page = 20;
        $equipmentsObj = thoseIndexed('equipment', $vars);
        $totalcount = $equipmentsObj->totalcount();
        $equipments = $equipmentsObj->limit((int)$start, $per_page);

        $totalpage = ceil($totalcount / $per_page);

        if ($start < $totalpage - 1) {
            $vars['next_start'] = $start + 1;
        } else {
            $vars['next_start'] = -1;
        }

        if (count($equipments)){
            $vars['equipments'] = $equipments;
        } else {
            $vars['equipments'] = [];
        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/equipment/equipments-ajax', $vars));
    }

    public function actionEdit($id=0)
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');

        $vars['ids'] = [(int)$id];
        $Requipments = thoseIndexed('equipment', $vars)->limit();
        $Requipment = (object)$Requipments[0];
        if (!$Requipment->id) {
        	$this->redirect('error/404');
        }

        $equipment = a('equipment', ['rid' => (int)$id]);

        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $validator = new \Gini\CGI\Validator;

            try {

                if (!$equipment->id) {
                    $equipment->rid = (int)$id;
                }
                $equipment->chargeRule = $form['chargeRule'];
                $equipment->openCalendar = $form['openCalendar'];
                $equipment->application = $form['application'];
                $equipment->frontShow = isset($form['frontShow']) ? 1 : 0;
                $equipment->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }

        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/equipment/equipment-edit-modal', [
            'equipment' => $equipment,
            'Requipment' => $Requipment
        ]));
    }

    public function actionEditImg($id=0)
    {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');

        $equipment = a('equipment', ['rid' => (int)$id]);

        if (!$equipment->id) {
            $equipment->rid = (int)$id;
            $equipment->save();
        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/equipment/equipment-edit-image-modal', [
            'equipment' => $equipment,
        ]));
    }

    public function actionUpload($id=0)
    {
        $form = $this->form('files');
        $file = $form['file'];
        $equipment = a('equipment', $id);
        if ($file) {
            $fileName = $file['name'];
            $fullPath = $equipment->filePath($fileName);
            \Gini\File::ensureDir($equipment->filePath());
            move_uploaded_file($file['tmp_name'], $fullPath);
            if (is_file($fullPath)) {
                return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                    'ok' => H(T('文件上传成功!')),
                    'file' => $fullPath
                ]);
            }
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
            'error' => H(T('文件上传失败!')),
            'file' => $file['name']
        ]);
    }

    //删除文件
    public function actionRemoveFile($id=0)
    {
        $form = $this->form('post');
        $name = H($form['name']);
        $equipment = a('equipment', $id);
        $fullPath = $equipment->filePath($name);
        if (file_exists($fullPath) && is_file($fullPath)) {
            \Gini\File::delete($fullPath);
        }
    }

}