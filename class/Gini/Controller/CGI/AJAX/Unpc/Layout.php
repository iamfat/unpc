<?php

namespace Gini\Controller\CGI\AJAX\Unpc;

class Layout extends \Gini\Controller\CGI {

	public function actionSelectType($index=0)
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		if ($form['submit']) {

			if (isset($form['type'])) {
				$layout = a('layout', ['index' => $index]);
				
				if (!$layout->id) {
					$layout->index = $index;
					$layout->save();
				}

				if ($layout->type != $form['type']) {
					$layout->type = $form['type'];
					$layout->save();
				}
				
				return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/html', [
					'mode' => 'contain',
					'selector' => ".gridster li[data-index={$index}] span.type",
					'html' => \Gini\ORM\Layout::$TYPE[$form['type']]
				]));
			}
			else {
				$form['_errors']['type'] = H(T('请选择您需要设置的布局类型!'));
			}
			
		}

		$vars = [
			'index' => $index,
			'form' => $form
		];
		$view = U('admin/config/layout/item', $vars);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
	}

	public function actionSetType($index=0)
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		if (!isset($form['type']) 
				|| $form['type'] == \Gini\ORM\Layout::TYPE_NAVBAR ) { return; }

		$vars = [
			'index' => $index,
			'form' => $form
		];
		$view = U('admin/config/layout/set-view', $vars);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
	}

	public function actionUpdateItems() 
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$items = (array)$form['items'];
			$indexes = [];
			foreach ($items as $k => $item) {
				$layout = a('layout', ['index' => $item['index']]);
				if (!$layout->id) $layout->index = $item['index'];
				$layout->type = $item['type'];
				$layout->sizex = $item['size_x'];
				$layout->sizey = $item['size_y'];
				$layout->col = $item['col'];
				$layout->row = $item['row'];
				$layout->save();
				$indexes[] = $item['index'];
			}

			if (count($indexes)) {
				those('layout')->whose('index')->isNotIn($indexes)->deleteAll();
			}
			else {
				those('layout')->deleteAll();			
			}

			\Model\Alert::setMessage(H(T('首页布局更新成功')), \Model\Alert::TYPE_OK); 
		}

		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
	}

	public function actionRefreshItem($index=0)
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/layout/items'));
	}



	//获取模块选择后数据
	public function actionUpdateCategory()
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		$categoryId = $form['id'];
		$category = a('category',$categoryId);
		//获取用户原有选择的样式,若是新创建的则默认为1
        $old_style = $form['style_num'] ?: 'one';

		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', V('admin/config/layout/indexs/module-icons', [
								'identity' => $category->identity, 'style' => $old_style,
            ]));
	}

	public function actionUpdateCategoryValue()
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		$objectDate = a('category',['name'=>'仪器列表']);
		$objectDate->style = $form['type'];
		$objectDate->source = $form['identity'];

		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/layout/module', [
                'object' => $objectDate,
              ]));
	}

	public function actionUpdateLogin()
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		$login = a('layout','26');
		$login->style = $form['type'];

		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/layout/login', [
                'object' => $login,
              ]));
	}

	public function actionUpdateLink()
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		$object = a('layout',['type'=>'4']);

		$object->style = $form['type'];
		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/layout/link', [
	              'object' => $object,
	            ]));
	}

	public function actionUpdateCategoryStyle()
	{
		if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		$form = $this->form('post');

		$category = a('category',$form['id']);
		$category->style = $form['type'];

		return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/layout/category-style', [
                'object' => $category,
              ]));
	}

	public function actionUpdateLayout() {
		$me = _G('ME');
		if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
		unset($form);
		$form = $this->form('post');

		$layout = a('layout',['index'=>$form['id']]);

		$view = $layout->render();

    }

    // 抓取数据选择模板
    public function actionSetPreview() {
        $me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form('post');
        $index = (int)$form['index'];
				$layout = a('layout')->whose('index')->is($index);
				if (!$layout->id) $this->redirect('error/404');
        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', J([
					'view' =>  (string)U('admin/config/layout/preview', [
							'l' => $layout,
					]),
					'col' => $layout->col,
					'row' => $layout->row,
					'x' => $layout->sizex,
					'y' => $layout->sizey
				]));

	}

}
