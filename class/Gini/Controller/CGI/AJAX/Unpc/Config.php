<?php

namespace Gini\Controller\CGI\AJAX\Unpc;

class Config extends \Gini\Controller\CGI {

	public function actionDesign($method='system') 
	{
            if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                  $form = $this->form('post');
                  switch ($method) {
                  	case 'system':
                  		$systemName = H($form['systemName']);
                  		$apiUrl = H($form['apiUrl']);
                          $loginUrl = H($form['loginUrl']);
                          $parsed = parse_url($apiUrl);
                          $url = isset($parsed['host']) ? $parsed['host'] : $_SERVER['HTTP_HOST'];
                          $url .= ($parsed['port'] != '80' ? (':'.($parsed['port'] ?: $_SERVER['SERVER_PORT'])) : '');

                          Hub('config.design.system', [
                                'name' => $systemName,
                                'url' => $apiUrl,
                                'baseUrl' =>  $url,
                                'loginUrl' => $loginUrl
                          ]);
                          \Model\Alert::setMessage(H(T('系统设置更新成功')), \Model\Alert::TYPE_OK);
                  		break;
                  	case 'color':
                  		$bgColor = H($form['bgColor']);
                  		$fontColor = H($form['fontColor']);
                  		$mainTitleColor = H($form['mainTitleColor']);
                  		$navBgColor = H($form['navBgColor']);
                  		$navBgActiveColor = H($form['navBgActiveColor']);
                  		$navFontColor = H($form['navFontColor']);
                        $navFontActiveColor = H($form['navFontActiveColor']);
                        $layoutBgColor = H($form['layoutBgColor']);
                        $moduleTitleColor = H($form['moduleTitleColor']);
                        Hub('config.design.color', [
                            'bg' => $bgColor,
                            'font' => $fontColor,
                            'mainTitle' => $mainTitleColor,
                            'navBg' => $navBgColor,
                            'navBgActive' => $navBgActiveColor,
                            'navFont' => $navFontColor,
                            'navFontActive' => $navFontActiveColor,
                            'layoutBgColor' => $layoutBgColor,
                            'moduleTitleColor' => $moduleTitleColor,
                            //导航栏阴影颜色
                            'activeTopBorder' => H($form['activeTopBorder']),
                            'activeBottomBorder' => H($form['activeBottomBorder']),
                            'activeBodyPrecent' => H($form['activeBodyPrecent'])
                        ]);
                        //进行编译。
                        $comiler = \Model\Widget::factory('Compiler');
                        $comiler->UpdateStyle();

                        \Model\Alert::setMessage(H(T('底色设计更新成功')), \Model\Alert::TYPE_OK);
                        break;
                  	default:
                  		break;
                  }

                  return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
                 
            }

            
	}

      public function actionNav()
      {
            if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                  $form = $this->form('post');
                  $nav = [
                        'position' => H($form['position']),
                        'hasShadow' => H($form['hasShadow']),
                        'shadowPosition' => H($form['shadowPosition']),
                        'activeBackground' => H($form['activeBackground']),
                  ];
                  if (isset($form['leftTop'])) {
                        $nav['leftTop'] = H($form['leftTop']);
                  }
                  if (isset($form['rightTop'])) {
                        $nav['rightTop'] = H($form['rightTop']);
                  }
                  if (isset($form['leftBottom'])) {
                        $nav['leftBottom'] = H($form['leftBottom']);
                  }
                  if (isset($form['rightBottom'])) {
                        $nav['rightBottom'] = H($form['rightBottom']);
                  }

                  Hub('config.nav', $nav) ?
                  \Model\Alert::setMessage(H(T('导航配置更新成功')), \Model\Alert::TYPE_OK) : 
                  \Model\Alert::setMessage(H(T('导航配置更新失败')), \Model\Alert::TYPE_ERROR);

                  return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
            }
      }

      public function actionNavModule()
      {
            if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                  $form = $this->form('post');

                  $modules = $form['modules'];
         
                  $modules = array_map(function($v){
                        return H($v);
                  }, $modules);

                  if (count($modules) < 3 || count($modules) > 9)
                  {
                        \Model\Alert::setMessage(H(T('请设置3~9个导航模块!')), \Model\Alert::TYPE_ERROR);
                  }
                  else {
                        Hub('config.nav.modules', $modules) ? 
                        \Model\Alert::setMessage(H(T('模块配置更新成功')), \Model\Alert::TYPE_OK) : 
                        \Model\Alert::setMessage(H(T('模块配置更新失败')), \Model\Alert::TYPE_ERROR);
                  }

                  return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
            }
      }

      public function actionNavLayout()
      {
        if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          $form = $this->form('post');
          // 更新导航模块
          $modules = $form['modules'];
         
          $modules = array_map(function($v){
                  return H($v);
            }, $modules);

        if (count($modules) < 3 || count($modules) > 9)
        {
          \Model\Alert::setMessage(H(T('请设置3~9个导航模块!')), \Model\Alert::TYPE_ERROR);
        }
        else {
          Hub('config.nav.modules', $modules);
        }

        // 更新下方设置
        $nav = [
                  'position' => H($form['position']),
                  'hasShadow' => H($form['hasShadow']),
                  'shadowPosition' => H($form['shadowPosition']),
                  'activeBackground' => H($form['activeBackground']),
                  'activeTopBorder' => H($form['activeTopBorder']),
                  'activeBottomBorder' => H($form['activeBottomBorder'])
                ];
        if (isset($form['leftTop'])) {
              $nav['leftTop'] = H($form['leftTop']);
        }
        if (isset($form['rightTop'])) {
              $nav['rightTop'] = H($form['rightTop']);
        }
        if (isset($form['leftBottom'])) {
              $nav['leftBottom'] = H($form['leftBottom']);
        }
        if (isset($form['rightBottom'])) {
              $nav['rightBottom'] = H($form['rightBottom']);
        }

        Hub('config.nav', $nav) ?
        \Model\Alert::setMessage(H(T('导航配置更新成功')), \Model\Alert::TYPE_OK) : 
        \Model\Alert::setMessage(H(T('导航配置更新失败')), \Model\Alert::TYPE_ERROR);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
        }
      }

      public function actionSEO() 
      {
            if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                  $form = $this->form('post');
                  $title = H($form[titleTag]);
                  $keywords = H($form[keywords]);
                  $description = H($form[description]);
                  $copyright = H($form[copyright]);
                  $author = H($form[author]);
                  Hub('config.seo', [
                        'titleTag' => $title,
                        'keywords' => $keywords,
                        'description' => $description,
                        'copyright' => $copyright,
                        'author' => $author
                  ]) ?
                  \Model\Alert::setMessage(H(T('SEO配置更新成功')), \Model\Alert::TYPE_OK) : 
                  \Model\Alert::setMessage(H(T('SEO配置更新失败')), \Model\Alert::TYPE_ERROR);

                  return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
            }
      }

      public function actionUploadShortcutIcon()
      {
            $form = $this->form('files');
            $file = $form['file'];
            $fullPath = APP_PATH.'/'.DATA_DIR."/shortcut/";
            if ($file) {
                  $fileName = $file['name'];
                  \Gini\File::ensureDir($fullPath);
                  $fullPath .= $fileName;
                  move_uploaded_file($file['tmp_name'], $fullPath);
                  if (is_file($fullPath)) {
                      Hub('shortcut.filename', $fileName); 
                      return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                          'ok' => H(T('文件上传成功!')),
                          'file' => $fullPath
                      ]);
                  }
            }

            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                  'error' => H(T('文件上传失败!')),
                  'file' => $file['name']
            ]);
      }

      public function actionRemoveShortcutIcon()
      {
            $form = $this->form('post');
            $name = H($form['name']);
            $fullPath = APP_PATH.'/'.DATA_DIR."/shortcut/{$name}";
            if (file_exists($fullPath) && is_file($fullPath)) {
                  Hub('shortcut.filename', '');
                  \Gini\File::delete($fullPath);
            }
      }

      public function actionUploadBackgroundIcon()
      {
            $form = $this->form('files');
            $file = $form['file'];
            $fullPath = APP_PATH.'/'.DATA_DIR."/background/";
            if ($file) {
                  $fileName = $file['name'];
                  \Gini\File::ensureDir($fullPath);
                  $fullPath .= $fileName;
                  move_uploaded_file($file['tmp_name'], $fullPath);
                  if (is_file($fullPath)) {
                      Hub('background.filename', $fileName); 
                      return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                          'ok' => H(T('文件上传成功!')),
                          'file' => $fullPath
                      ]);
                  }
            }

            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                  'error' => H(T('文件上传失败!')),
                  'file' => $file['name']
            ]);
      }

      public function actionRemoveBackgroundIcon()
      {
            $form = $this->form('post');
            $name = H($form['name']);
            $fullPath = APP_PATH.'/'.DATA_DIR."/background/{$name}";
            if (file_exists($fullPath) && is_file($fullPath)) {
                  Hub('background.filename', '');
                  \Gini\File::delete($fullPath);
            }
      }

      public function actionUpdateBackgroundAttr()
      {
            if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                  $form = $this->form('post');
                  Hub('background.repeat', H($form['repeat']));
                  Hub('background.position', H($form['position']));
                  \Model\Alert::setMessage(H(T('系统背景图更新成功')), \Model\Alert::TYPE_OK);
                  if (Hub('background.filename')) {
                        //进行编译。
                        $comiler = \Model\Widget::factory('Compiler');
                        $comiler->UpdateStyle();
                  }
                  return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
            }
      }




}
