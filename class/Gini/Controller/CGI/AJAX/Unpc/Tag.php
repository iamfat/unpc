<?php

namespace Gini\Controller\CGI\AJAX\Unpc;

class Tag extends \Gini\Controller\CGI
{
    public function actionAdd($identity='')
    {
        $form = $this->form();
        $category = a('category', ['identity' => $identity]);
        if (!$identity || !$category->id) return FALSE;
        if ($category->parent->id) return FALSE;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        	$validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('name', H($form['name']), T('名称不能为空!'))
                    ->validate('identity', H($form['identity']), T('标示名不能为空!'))
                    ->validate('identity', preg_match('/^[a-zA-Z]+$/', H($form['identity'])), T('标示名需要为英文!'))
                    ->done();

                $tag = a('tag');
                $tag->category = $category;
                $tag->name = H($form['name']);
                $tag->identity = H($form['identity']);
                $tag->parent = a('tag', H($form['parent_id']));
                $tag->root = a('tag', H($form['root_id']));
                $tag->type = (int)$form['type'];
                $tag->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script>window.location.reload()</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $vars = [
            'category' => $category,
            'form' => $form
        ];

        $view = U('admin/tag/add', $vars);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionEdit($id=0)
    {
        $form = $this->form();
        $tag = a('tag', $id);
        if (!$tag->id) return FALSE;
        $category = $tag->category;
        if (!$category->id) return FALSE;
        if ($category->parent->id) return FALSE;
        //POST是修改标签数据 如果是GET是展示修改界面的html
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('name', H($form['name']), T('名称不能为空!'))
                    ->validate('identity', H($form['identity']), T('标示名不能为空!'))
                    ->validate('identity', preg_match('/[a-zA-Z]*/i', H($form['identity'])), T('标示名需要为英文!'))
                    ->done();

                $tag->name = H($form['name']);
                $tag->identity = H($form['identity']);
                $tag->parent = a('tag', H($form['parent_id']));
                $tag->root = a('tag', H($form['root_id']));
                $tag->type = (int)$form['type'];
                $tag->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script>window.location.reload()</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $vars = [
            'category' => $category,
            'tag' => $tag,
            'form' => $form
        ];

        $view = U('admin/tag/edit', $vars);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionDelete($id=0) 
    {
        $form = $this->form();
        $tag = a('tag', $id);
        if (!$tag->id) return FALSE;
        $tag->delete();
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script>window.location.reload()</script>');

    }
}
