<?php

namespace Gini\Controller\CGI\AJAX\Unpc;

class Module extends \Gini\Controller\CGI {

	public function actionEquipment($action='setting')
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$form = $this->form('post');
			switch ($action) {
              	case 'setting':
					Hub('Module.equipment.setting', [
						'root' => H($form['root']),
						'position' => H($form['position']),
						'Loading' => H($form['listLoading']),
						'selectTags' => $form['tags'],
						'tagIDs' => $form['ids'],
						'listShowStyle' => H($form['listShowStyle']),
						'rootViewStyle' => H($form['rootViewStyle'])
					]);
					\Model\Alert::setMessage(H(T('仪器展示设置更新成功')), \Model\Alert::TYPE_OK);
              		break;
              	default:
              		break;

            }

            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());

		}
	}

	public function actionSetting($identity='')
	{
		$category = a('category', ['identity' => $identity]);
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$form = $this->form('post');
			$type = H($form['displayType']);
			$display = a('display', ['identity' => $identity]);
			if (!$display->id) { $display->identity = $identity; }
			$display->type = $type;

			/*
			 * displayType暂时都取1  指的是列表
			 * displayType 为2 指的是文章
			 * 仅处理 type为1 的情况下排序的顺序设置
			 */
			switch ($type) {
				case \Gini\ORM\Display::TYPE_LIST:
					$display->listTitle = H($form['listTitle']);
					$display->listTitleBackground = H($form['listTitleBackground']);
					$display->listTitleBackgroundColor = H($form['listTitleBackgroundColor']);
					$display->listTitleRef = H($form['listTitleRef']);
					$display->listLoading = H($form['listLoading']);
					$display->listSort = H($form['listSort']);
                    $display->listOrder = H($form['listOrder']);
					$display->hasSecond = (int)$form['hasSecond'];
					$display->secondPosition = H($form['secondPosition']);
					$display->tagPosition = H($form['tagPosition']);
					$display->listTitlelink = $form['listTitlelink'];
					break;
				case \Gini\ORM\Display::TYPE_POST:
                    $display->listTitle = H($form['listTitle']);
                    $display->listTitleBackground = H($form['listTitleBackground']);
                    $display->listTitleBackgroundColor = H($form['listTitleBackgroundColor']);
                    $display->listTitleRef = H($form['listTitleRef']);
                    $display->listLoading = H($form['listLoading']);
                    $display->listSort = H($form['listSort']);
                    $display->listOrder = H($form['listOrder']);
                    $display->hasSecond = (int)$form['hasSecond'];
                    $display->secondPosition = H($form['secondPosition']);
                    $display->tagPosition = H($form['tagPosition']);
					$display->postShow = H($form['postShow']);
					$display->listTitlelink = $form['listTitlelink'];
					break;
				default:
					/* Do Nothing */
					/*
					 * type为空的情况
					 */
					$display->postShow = H($form['postShow']);
					$display->listTitlelink = $form['listTitlelink'];
					break;
			}
            //如果listTitle为文件模式则clickAction为下载 否则为进入文章
            if($display->listTitle=='extension')
            {
                $display->clickAction='download';
            }
            else
            {
                $display->clickAction='redirect';
            }
			$display->save() ?
			\Model\Alert::setMessage(H(T('设置更新成功')), \Model\Alert::TYPE_OK) :
			\Model\Alert::setMessage(H(T('设置更新失败')), \Model\Alert::TYPE_ERROR);

			if ($display->type == \Gini\ORM\Display::TYPE_LIST) {
				if ($display->listTitle == 'extension') {
					$category->type = \Gini\ORM\Category::TYPE_FILE;
				}
				else {
					$category->type = \Gini\ORM\Category::TYPE_POST;
				}
			}
			elseif ($display->type == \Gini\ORM\Display::TYPE_POST) {
				$category->type = \Gini\ORM\Category::TYPE_POST;
			}
			else {
				$category->type = \Gini\ORM\Category::TYPE_POST;
			}
			$category->save();
			return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
		}
	}

	public function actionAdd()
	{
		$me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        	$validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('name', H($form['name']), T('名称不能为空!'))
                    ->validate('identity', H($form['identity']), T('标示名不能为空!'))
                    ->validate('identity', preg_match('/^[a-zA-Z]+$/', $form['identity']), T('标示名需要为英文!'))
                    ->done();
                $module = a('category');
                $module->name = H($form['name']);
                $module->identity = H($form['identity']);
                $module->icon = H($form['icon']);
                $module->weight = (int)$form['weight'];
                $module->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = U('admin/module/add-module-modal', ['form' => $form]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
	}

	public function actionEdit($identity)
	{
		$me = _G('ME');
        if (!$me->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();
        $module = a('category', ['identity' => $identity]);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        	$validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('name', H($form['name']), T('名称不能为空!'))
                    ->validate('identity', H($form['identity']), T('标示名不能为空!'))
                    ->validate('identity', preg_match('/^[a-zA-Z]+$/', H($form['identity'])), T('标示名需要为英文!'))
                    ->done();

                $module->name = H($form['name']);
                $module->identity = H($form['identity']);
                $module->icon = H($form['icon']);
                $module->weight = (int)$form['weight'];
                $module->save();

                return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = U('admin/module/edit-module-modal', [
        	'form' => $form,
        	'module' => $module
        ]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
	}

	public function actionDelete($identity)
	{
		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			$form = $this->form();
			$module = a('category', ['identity' => $identity]);

      $module->delete();
      return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
		}
	}

    public function actionSortable()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $form = $this->form('post');
            $ids = $form['ids'];
            $ids = explode(',', $ids);
            foreach ($ids as $k => $id) {
        		$module = a('category', $id);
                $module->weight = $k;
                $module->save();
            }
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', '<script data-ajax="true">window.location.reload();</script>');
        }
    }

    public function actionUpdateStyle($identity='')
    {
    	$category = a('category', ['identity' => $identity]);
    	if (!$category->id) $this->redirect('error/401');
    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		$form = $this->form('post');
    		$category->style = H($form['style']);
    		$category->save() ?
			\Model\Alert::setMessage(H(T('样式设置更新成功')), \Model\Alert::TYPE_OK) :
			\Model\Alert::setMessage(H(T('样式设置更新失败')), \Model\Alert::TYPE_ERROR);

			return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
    	}

    }
}
