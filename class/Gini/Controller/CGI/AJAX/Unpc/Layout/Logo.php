<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class Logo extends \Gini\Controller\CGI {

	public function actionUpload($index=0) 
	{
		$form = $this->form('files');
        $file = $form['file'];
        $layout = a('layout', ['index' => $index]);
        if ($file) {
            $fileName = $file['name'];
            $fullPath = $layout->filePath($fileName);
            \Gini\File::ensureDir($layout->filePath());
            move_uploaded_file($file['tmp_name'], $fullPath);
            if (is_file($fullPath)) {
                return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
                    'ok' => H(T('文件上传成功!')),
                    'file' => $fullPath
                ]);
            }
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', [
            'error' => H(T('文件上传失败!')),
            'file' => $file['name']
        ]);
	}

    public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->proportion = H($form['proportion']);
            $layout->save();
        }
        \Model\Alert::setMessage(H(T('更新成功,点击返回布局查看效果')), \Model\Alert::TYPE_OK); 
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
    }

    public function actionRemoveFile($index=0)
    {
        $form = $this->form('post');
        $name = H($form['name']);
        $layout = a('layout')->whose('index')->is((int)$index);
        $fullPath = $layout->filePath($name);
        if (file_exists($fullPath) && is_file($fullPath)) {
            \Gini\File::delete($fullPath);
        }
    }
}