<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class Link extends \Gini\Controller\CGI {
    /**
     * @param int $number
     * @return mixed|object
     * 输出一行输入框
     * 传入参数在模版中体现 对应链接id
     */
	public function actionAdd($number=0) 
	{
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/config/layout/indexs/link-item', [
                'number' => $number
            ]));
	}

    /**
     * @param int $index
     * 更新 link 的显示
     * 可以只有title 没有连接
     */
    public function actionUpdate($index=0)
    {
    	$form = $this->form('post');
    	if ($form['submit']) {
    		$layout = a('layout', ['index' => $form['index']]);
    		$layout->type = $form['type'];

    		$links = [];
    		foreach ((array)$form['title'] as $k => $title) {
                if (!$title) continue;
    			$links[] = [
    				'title' => H($title),
    				'url' => H($form['url'][$k]),
                    'icon' => H($form['icon'][$k])
    			];
            }
    		$layout->links = $links;
            $layout->align = H($form['align']);
            $layout->style = H($form['style']);
            $layout->height = (int)$form['height'];
            $layout->global = (int)$form['global'];
            $layout->backgroundColor = $form['backgroundColor'];
            $layout->fontColor = $form['fontColor'];
            $layout->center_show = (int)$form['center_show'];
    		$layout->save() ?
                \Model\Alert::setMessage(H(T('连接模块更新成功')), \Model\Alert::TYPE_OK) :
                \Model\Alert::setMessage(H(T('连接模块更新失败')), \Model\Alert::TYPE_ERROR);

			return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
    	}
    }
}