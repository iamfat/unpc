<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class EQpic extends \Gini\Controller\CGI {

	public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->size = H($form['size']);
            $layout->showEQName = H($form['showEQName']);
            $layout->count = (int)$form['count'];
            $layout->float = H($form['float'] ?: 'left');
            $layout->isScroll = H($form['isScroll']);
            $layout->scrollNumber = (int)$form['scrollNumber'];
            $layout->save();
        }
    }
}