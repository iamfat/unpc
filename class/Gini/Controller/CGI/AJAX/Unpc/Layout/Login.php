<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class Login extends \Gini\Controller\CGI {

	public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->style = H($form['style']);
            $layout->hide = (bool)$form['hide_login'];
            $layout->height = (int)$form['height'];
            $layout->direction = (bool)$form['direction_login'];
            $layout->loginUrl = H($form['loginUrl']);
            $layout->save();
        }
        \Model\Alert::setMessage(H(T('更新成功,点击返回布局查看效果')), \Model\Alert::TYPE_OK); 
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
    }
}