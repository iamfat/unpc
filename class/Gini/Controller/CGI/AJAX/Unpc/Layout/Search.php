<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class Search extends \Gini\Controller\CGI {

	public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->index = (int)$form['index'];
            $layout->height = (int)$form['height'];
            $layout->float = H($form['float']);
            $layout->save();
        }
        \Model\Alert::setMessage(H(T('更新成功,点击返回布局查看效果')), \Model\Alert::TYPE_OK); 
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
    }
}