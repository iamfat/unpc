<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class Module extends \Gini\Controller\CGI {

	public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->source = H($form['source']);
            $layout->count = (int)$form['count'];
            $layout->listTitle = H($form['listTitle']);
            // $layout->listTitleRef = H($form['listTitleRef']);
            $layout->clickAction = H($form['clickAction']);
            $layout->listSort = H($form['listSort']);
            $layout->listOrder = H($form['listOrder']);
            $layout->style = H($form['style']);
            $layout->style_type = H($form['style_type']);
            $layout->height = (int)$form['height'];
            $layout->float = H($form['float'] ?: 'left');
            $layout->save();
            \Model\Alert::setMessage(H(T( '系统模块设置成功' )), \Model\Alert::TYPE_OK); 
            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', \Model\Alert::getMessage());
        }
    }

    public function actionGetModuleIcons()
    {
        $form = $this->form();

        $is_post = $form['style_type'] == 'post';

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('components/html', [
            'html' => U('admin/config/layout/indexs/module-icons', [
                    'is_post' => $is_post,
                    'style' => $form['style']
                ]),
            'mode' => 'contain',
            'selector' => $form['selector']
        ]));  

    }
}