<?php

namespace Gini\Controller\CGI\AJAX\Unpc\Layout;

class UserStatus extends \Gini\Controller\CGI {

	public function actionUpdate($index=0) 
    {
        $layout = a('layout', ['index' => $index]);
        $form = $this->form('post');
        if ($form['submit']) {
            $layout->index = (int)$form['index'];
            $layout->height = (int)$form['height'];
            $layout->save();
        }
    }
}