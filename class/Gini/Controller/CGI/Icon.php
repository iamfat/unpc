<?php

namespace Gini\Controller\CGI;

class Icon extends Index {

    public function __index(){

        $form = $this->form();
        $file = $form['f'];

        list($category, $file) = explode(':', $file, 2);
        if (!$file) {
            $file = $category;
            $category = NULL;
        }

        //检查 !module/path 格式
        if (preg_match ('/^\!(.*?)(?:\/(.+))?$/', $file, $matches)) {
            $category = $matches[1] ?: NULL;
            $file = $matches[2];
        }

        switch ($category) {
            case 'category':
                $files = explode('/', $file);
                if ($files[0] == 'icon') {
                    $layout = a('layout', ['id' => $files[1]]);
                    if (!$layout->id) exit();
                    $filePath = $layout->filePath($files[2]);
                    $this->_sendFile($filePath);
                    // $expiresString = date(date_rfc822, strtotime('1 day'));
                    // header('cache-control: private, max-age=10800, pre-check=10800');
                    // header('pragma: private');
                    // header('expires: '. $expiresString);
                    // header('Content-Type: '. \Gini\File::mimeType($filePath) );
                    // @readfile($filePath);
                }
                break;
            case 'platform':
                $files = explode('/', $file);
                if ($files[0] == 'icon') {
                    if (!$files[1]) exit();
                    $filePath = \Model\Sdu\Platform::imagePath($files[1], $files[2], $files[3]);
                    $this->_sendFile($filePath);
                }
                break;
            case 'content':
                $files = explode('/', $file);
                if ($files[0] == 'icon') {
                    $content = a('content', ['id' => $files[1]]);
                    if (!$content->id) exit();
                    $filePath = $content->filePath($files[2]);
                    $this->_sendFile($filePath);
                }
                break;
            case 'equipment':
                $files = explode('/', $file);
                if ($files[0] == 'icon') {
                    $equipment = a('equipment', ['id' => $files[1]]);
                    if (!$equipment->id) exit();
                    $filePath = $equipment->filePath($files[2]);
                    $this->_sendFile($filePath);
                }
                break;
            default:
                $files = explode('/', $file);
                switch ($files[0]) {
                    default:
                        $filePath = APP_PATH.'/'.DATA_DIR."/{$category}/$file";
                        $expiresString = date(date_rfc822, strtotime('1 day'));
                        $this->_sendFile($filePath);
                        // header('cache-control: private, max-age=10800, pre-check=10800');
                        // header('pragma: private');
                        // header('expires: '. $expiresString);
                        // header('Content-Type: '. \Gini\File::mimeType($filePath) );
                        // @readfile($filePath);
                        // exit();
                        break;
                }
                break;
        }

        exit();

    }

    /**
     * 发送文件
     *
     * @param string   $fileName      文件名称或路径
     * @param string   $fancyName     自定义的文件名,为空则使用filename
     * @param boolean  $forceDownload 是否强制下载
     *
     * @return boolean
     */
    private function _sendFile($fileName, $fancyName = '', $forceDownload = false)
    {
        if (!is_readable($fileName)) {
            header("HTTP/1.1 404 Not Found");
            die();
        }
        $fileStat = stat($fileName);
        $lastModified = $fileStat['mtime'];

        $md5 = md5($fileStat['mtime'] .'='. $fileStat['ino'] .'='. $fileStat['size']);
        $etag = '"' . $md5 . '-' . crc32($md5) . '"';
        header('Last-Modified: ' . gmdate("D, d M Y H:i:s", $lastModified) . ' GMT');
        header("ETag: $etag");

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $lastModified) {
            header("HTTP/1.1 304 Not Modified");
        }
        if (isset($_SERVER['HTTP_IF_UNMODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_UNMODIFIED_SINCE']) < $lastModified) {
            header("HTTP/1.1 304 Not Modified");
        }
        if (isset($_SERVER['HTTP_IF_NONE_MATCH']) &&  $_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
            header("HTTP/1.1 304 Not Modified");
            return true;
        }
        if ($fancyName == '') {
            $fancyName = basename($fileName);
        }

        $fileSize = $fileStat['size'];

        $contentLength = $fileSize;
        $isPartial = false;
        if (isset($_SERVER['HTTP_RANGE'])) {
            if (preg_match('/^bytes=([0-9]*)-([0-9]*)?$/', $_SERVER['HTTP_RANGE'], $matches)) {
                $startPos = $matches[1];
                $endPos = $matches[2];
                if ($startPos == '' && $endPos == '')
                {
                    return false;
                }

                if ($startPos == '')
                {
                    $startPos = $fileSize - $endPos;
                    $endPos = $fileSize - 1;
                }
                else if ($endPos == '')
                {
                    $endPos = $fileSize - 1;
                }
                $startPos = $startPos < 0 ? 0 : $startPos;
                $endPos = $endPos > $fileSize - 1 ? $fileSize - 1 : $endPos;
                $length = $endPos - $startPos + 1;
                if ($length < 0)
                {
                    return false;
                }
                $contentLength = $length;
                $isPartial = true;
            }
        }

        // send headers
        if ($isPartial)
        {
            header('HTTP/1.1 206 Partial Content');
            header("Content-Range: bytes $startPos-$endPos/$fileSize");

        }
        else
        {
            header("HTTP/1.1 200 OK");
            $startPos = 0;
            $endPos = $contentLength - 1;
        }
        header('Pragma: cache');
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Accept-Ranges: bytes');
        header('Content-type: ' . \Gini\File::mimeType($fileName));
        header('Content-Length: ' . $contentLength);

        if ($forceDownload)
        {
            header('Content-Disposition: attachment; filename="' . rawurlencode($fancyName). '"');//汉字自动转为URL编码
            header('Content-Disposition: attachment; filename="' . $fancyName. '"');
        }
        header("Content-Transfer-Encoding: binary");

        $bufferSize = 2048;
        $bytesSent = 0;
        $fp = fopen($fileName, "rb");
        fseek($fp, $startPos);
        //fpassthru($fp);

        while ($bytesSent < $contentLength && !feof($fp) && connection_status() == 0 )
        {
            $readBufferSize = $contentLength - $bytesSent < $bufferSize ? $contentLength - $bytesSent : $bufferSize;
            $buffer = fread($fp, $readBufferSize);
            echo $buffer;
            ob_flush();
            flush();
            $bytesSent += $readBufferSize;
        }
        exit();
    }
}