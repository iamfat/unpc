<?php

namespace Gini\Controller\CGI;

class Category extends Layout\Unpc {

    function __index($module='equipment', $secondary='')
    {
        $form = $this->form();
        $vars = [
            'form' => $form,
            'module' => $module
        ];

        switch ($module) {
            case 'equipment':
                // 暂时屏蔽掉需要进行 CSRF 安全防护的位置
                // $x = md5($_SESSION['token']);
                // if (count($form) && H($form['token']) != md5($_SESSION['token'])) {
                //     $this->redirect('error/401');
                // }
                $view = 'equipment/view';
                $vars['u'] = H(\Gini\URI::url(''));
                $vars['token'] = $_SESSION['token'] = 'category_equipment_search_'.uniqid();
                break;
            default:
                $view = 'category/view';
                break;
        }

        $category = a('category', ['identity' => H($module)]);
        $d = a('display')->whose('identity')->is(H($module));

        if (!$category->id) $this->redirect('error/404');

        if ($d->hasSecond) {
            //设置默认stag
            $stag = those('tag')->whose('category')->is($category)->current();
        }
        //下面这句一般无用，因为不用url传值，多靠路由。
        $tagName = H($secondary ?: $form['tag']);
        if ($tagName) {
            $tag = a('tag')->whose('identity')->is($tagName);
            $stag = $tag->id ? $tag : $stag;
        }

        if ($stag->id) {
            $url = URL("/category/{$category->identity}/{$stag->identity}", ['token' => md5($vars['token'])]);
        }
        else {
            $url = URL("/category/{$category->identity}", ['token' => md5($vars['token'])]);
        }
        $vars['u'] = $url;

        if ($d->id && $d->listLoading == 'page') {
            $per_page = \Gini\Config::get('system.pageCount') ?: 20;
            $db = a('post')->db();
            $where = [];
            $params = [];

            $SQL = "SELECT COUNT(P.id) FROM post AS P ";

            $where[] = " P.category_id = :category_id ";
            $params[':category_id'] = (int)$category->id;

            $where[] = " P.status = :status ";
            $params[':status'] = \Gini\ORM\Post::STATUS_PUBLISH;

            if ($stag->id) {
                $SQL .= "JOIN post_tag AS PT ON PT.post_id = P.id ";
                $where[] = " PT.tag_id = :tag_id ";
                $params[':tag_id'] = (int)$stag->id;
            }

            if (count($where)) {
                $SQL .= 'WHERE ' . join(' AND ', $where);
            }

            if ($d->listSort == 'initial') {
                $SQL .= " ORDER BY P.title ";
            }
            else {
                $SQL .= " ORDER BY P.pub_time ";
            }
            if ($d->listOrder =='asc')
            {
                $SQL .= "asc ";
            }
            else {
                $SQL .= "desc ";
            }

            $totalCount = (int)$db->value($SQL, null, $params);
            $start = (int)$form['st'];
            $start = $start - ($start % $per_page);
            if($start > 0) {
                $last = floor($totalCount / $per_page) * $per_page;
                if ($last == $totalCount) $last = max(0, $last - $per_page);
                if ($start > $last) {
                    $start = $last;
                }
            }
            $vars['pagination'] = \Model\Widget::factory('pagination', [
                'start' => $start,
                'per_page' => $per_page,
                'total' => $totalCount
            ]);
        }
        $setting = (array)Hub('Module.equipment.setting');
        if ($category->identity == 'equipment' && $setting['Loading'] == 'page') {
            class_exists('\Gini\ThoseIndexed');
            $vars['searchtext'] = HH(safe($form['keywords']));
            // 仪器是否在前台显示
            $showEqIds = those('equipment')->whose('frontShow')->is(1);
            $vars['ids'] = $showEqIds->totalCount() ? $showEqIds->get('rid') : '-1';
            $name = $setting['root'] == 'tag' ? 'cat' : 'group';
            if ($form['id']) {
                $vars[$name] = (int)$form['id'];
            }
            else {
                // $tagIDs = $setting['tagIDs'];
                // // 暂时只支持单个rootname
                // $tagName = $setting['selectTags'][0];
                // $id = (int)array_flip($tagIDs)[$tagName];
                // $vars[$name] = $id;
            }
            try {
                $equipments = thoseIndexed('equipment', $vars);
                $totalCount = $equipments->totalCount();
                $start = (int)$form['st'];
                $per_page = 10;

                $vars['pagination'] = \Gini\Module\Help::pagination($equipments, $start, $per_page, $vars['u'], $form);
                $vars['equipments'] = $equipments;
            }
            catch(Exception $e) {
                $vars['equipments'] = [];
            }
        }
        $vars['stag'] = $stag;
        $vars['per_page']=$per_page;
        $this->view->body = U($view, $vars);
    }

    function actionIcon($id=0, $file='')
    {
        $layout = a('layout', ['id' => $id]);
        if (!$layout->id || !$file) return;
        ob_clean();
        $filePath = $layout->filePath($file);
        $expiresString = date(date_rfc822, strtotime('1 day'));
        header('cache-control: private, max-age=10800, pre-check=10800');
        header('pragma: private');
        header('expires: '. $expiresString);
        header('Content-Type: image/jpeg');
        @readfile($filePath);
        exit();
    }

}
