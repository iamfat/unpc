<?php

namespace Gini\Controller\CGI;

class Equipment extends Layout\Unpc {

    function __index($id=0)
    {
        $vars = [];
        try {
            $sys = (array)Hub('config.design.system');
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");

            // 仪器是否在前台显示
            $showEqIds = those('equipment')->whose('frontShow')->is(1)
                ->andWhose('rid')->is($id);
            if (!$showEqIds->totalCount()) {
                throw new \Exception();
            }
            $vars['ids'] = $showEqIds->totalCount() ? $showEqIds->get('rid') : '-1';
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $return = $rpc->equipment->searchEquipments(['ids' => $id]);
            $equipment = $rpc->equipment->getEquipment($return['token']);
            $vars['equipment'] = $equipment;
        } catch(\Exception $e){
            $this->redirect('error/404');
        }
        $this->view->body = U('equipment/info', $vars);
    }

}
