<?php

namespace Gini\Controller\CGI\Layout;

abstract class Login extends \Gini\Controller\CGI\Layout {

    protected static $layout_name = 'login/layout';

    function __postAction($action, &$params, $response) {

        $route = \Gini\CGI::route();

        if ($route) $args = explode('/', $route);
        if (!$route || count($args) == 0) $args = ['index'];

        $this->view->header = U('login/header', ['selected'=>$args[0]]);
        $this->view->footer = U('login/footer');

        $args = array_slice($args, 0, 2);
        
        $class = [];
        $c = Safe(urldecode($args[0]));
        $this->view->layout_class = "layout-$c";

        
        return parent::__postAction($action, $params, $response);
    }
}
