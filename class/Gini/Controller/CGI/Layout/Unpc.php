<?php

namespace Gini\Controller\CGI\Layout;

abstract class Unpc extends \Gini\Controller\CGI\Layout {

    function __preAction($action, &$params)
    {
        parent::__preAction($action, $params);
        $this->view = U('layout');
        $this->view->title = \Gini\Config::get('layout.title');
        if (!$_SESSION['VISIT']) {
            $_SESSION['VISIT'] = true;
            $count = (int)Hub('visit.count');
            $count += 1;
            Hub('visit.count', $count);
        }
    }


    function __postAction($action, &$params, $response) {

        $this->view->seoSetting = (array)Hub('config.seo');
        $this->view->sysSetting = (array)Hub('config.design.system');

        $route = \Gini\CGI::route();
        if ($route) $args = explode('/', $route);

        if (!$route || count($args) == 0) {
            $args = ['index'];
        }
        $args = array_slice($args, 0, 2);

        $categories = those('category')->get('identity');

        if ($args[0] != 'index'
        && !($args[0] == 'category' && in_array($args[1], $categories))
        //这里route除了[!category,[icon......]]没找到还会出现啥情况，[icon....]待补充
        && !($args[0] == '!category' && in_array($args[1], ['icon']))
        ) {
            $args = ['index'];
        }

        //$this->view->header = U('header', ['selected'=>$args[0]]);
        $this->view->footer = U('footer');
        $this->view->header = U('header-list');

        $class = [];
        $c = Safe(urldecode($args[0]));
        $this->view->layout_class = "layout-$c";

        return parent::__postAction($action, $params, $response);
    }
}
