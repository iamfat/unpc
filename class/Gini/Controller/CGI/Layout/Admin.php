<?php

namespace Gini\Controller\CGI\Layout;

abstract class Admin extends \Gini\Controller\CGI\Layout {

    protected static $layout_name = 'admin/layout';
    
    function __postAction($action, &$params, $response) 
    {

        if (!\Gini\Auth::isLoggedIn()) {
            $this->redirect('/entry');
        }
        $this->view->title = \Gini\Config::get('admin.title');
        $this->view->sidebar = U('admin/sidebar', ['selected'=>$args[0]]);
        $this->view->nav    = U('admin/nav');
        $this->view->footer = U('admin/footer');
        
        $route = \Gini\CGI::route();
        if ($route) $args = explode('/', $route);
        if (!$route || count($args) == 0) $args = ['index'];

        $class = [];
        $c = Safe(urldecode($args[0]));
        $this->view->layout_class = "layout-$c";

        return parent::__postAction($action, $params, $response);
    }
}
