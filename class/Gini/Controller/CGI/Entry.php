<?php

namespace Gini\Controller\CGI;

class Entry extends Layout\Login {

    function __index() {
        if (\Gini\Auth::isLoggedIn()) {
            $this->redirect('command/config');
        }
        $form = $this->form();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $validator = new \Gini\CGI\Validator;

            try {

                $validator
                    ->validate('token', crypt($_SESSION['token'], $form['token']) == $form['token'], T('加密参数验证失败，请重新提交!'))
                    ->validate('AuthToken', $form['AuthToken'], T('登录名不能为空!'))
                    ->validate('AuthCode', $form['AuthCode'], T('密码不能为空!'));
                $username = \Gini\Auth::normalize($form['AuthToken']);
                $rule = \Gini\Config::get('login.failed.attempt.rule');
                if ($rule) {
                    $attempt = (array)Hub("login.failed.attempt.user.{$username}");

                    // 输入错误次数超过限制
                    if ($attempt['attempt'] >= $rule['attempt']) {
                        // 在限制时间内，不能登录
                        if (time() - $attempt['lastfailtime'] < $rule['failtime']) {
                            $timeStr = round(($rule['failtime'] - time() + $attempt['lastfailtime']) / 60);
                            $validator->validate(
                                'AuthCode',
                                false,
                                T("密码连续错误{$rule['attempt']}次! 请{$timeStr}分钟后再试!")
                            );
                        }
                        // 限制时间外，清空尝试记录
                        else {
                            Hub("login.failed.attempt.user.{$username}", [
                                'attempt' => 0
                            ]);
                        }
                    }
                }

                $validator
                    ->done();

                /*
                 * 生成一个 Auth 来验证用户
                 */
                $auth = \Gini\IoC::construct('\Gini\Auth', $username);
                $password = $form['AuthCode'];
                if ($auth->verify($password)) {
                    if ($rule) {
                        Hub("login.failed.attempt.user.{$username}", [
                            'attempt' => 0
                        ]);
                    }
                    unset($_SESSION['token']);

                    \Gini\Auth::login($username);

                    if ($form['remember-me']) {
                        \Gini\Module\Unpc::rememberMe();
                    }

                    if (isset($_SESSION['#LOGIN_REFERER'])) {
                        $referer = $_SESSION['#LOGIN_REFERER'] ?: null;
                        unset($_SESSION['#LOGIN_REFERER']);
                        if ($referer) {
                            $this->redirect($referer);
                        }
                    }

                    $this->redirect('/command/config');
                }
                elseif ($rule) {
                    Hub("login.failed.attempt.user.{$username}", [
                        'attempt' => $attempt['attempt'] + 1,
                        'lastfailtime' => time()
                    ]);
                }

                $form['_errors']['*'] = T('用户名/密码错误!');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
                if (isset($form['_errors']['token'])) {
                    unset($_SESSION['token']);
                    $this->redirect('error/401');
                }
            }

            $form['Password'] = '';
            foreach ($form['_errors'] as $message) {
                \Model\Alert::setMessage($message, \Model\Alert::TYPE_ERROR);
            }
        }

        $token = $_SESSION['token'] = 'admin_login_'.uniqid();

        $this->view->title = '用户登录';
        $this->view->body = U('login/index', [
            'form' => $form,
            'token' => $token
        ]);

    }


}
