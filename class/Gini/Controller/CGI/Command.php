<?php

namespace Gini\Controller\CGI;

class Command extends Layout\Admin
{

    function __index()
    {

        if ( _G('ME')->isAllowedTo('管理') ) {
            $this->actionConfig();
        }
        else {
            $this->actionModules();
        }
    }

    function actionUser($type='list')
    {
        if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/user/list', [
                'form' => $form,
              ]));
        } else{
          $this->view->body = U('admin/index')
                            ->set('content', U('admin/user/list', [
                                'form' => $form
                            ]));
        }   
    }

    function actionConfig($config='design')
    {
      if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
    	$form = $this->form();

      if ($form['type'] == 'ajax'){
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('admin/config/'.$config, [
                'form' => $form,
              ]));
      } else {
        $this->view->body = U('admin/index')
                ->set('content', U('admin/config/'.$config, [
                    'form' => $form
                ]));
      }
    }

    function actionModules($index="list")
    {

      $form = $this->form();

      $modules = _G('MODS');

      $vars = [
          'form' => $form,
          'index' => $index,
          'modules' => $modules
      ];

      switch ($index) {
          default:
              $view = 'admin/module/list';
              break;
      }

      if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view,$vars));
      }else{
        $this->view->body = U('admin/index')
                ->set('content', U($view, $vars));
      }
    }

    function actionModule($module="equipment", $index="setting")
    {
    	$form = $this->form();

        $vars = [
            'form' => $form,
            'module' => $module,
            'index' => $index
        ];

        $me = _G('ME');

        if (!$me->isAllowedTo('管理') && $index == 'setting') $index = 'list';

        switch ($module) {
            case 'equipment':
                $vars['api'] = Hub('config.design.system')['url'];
                $view = 'admin/module/equipment';
                break;
            default:
                $vars['category'] = a('category', ['identity' => $module]);
                $display = a('display', ['identity' => $module]);
                if (!$display->id) $display->identity = $module;
                $vars['display'] = $display;
                $view = "admin/module/template/{$index}";
                break;
        }
        switch ($index) {
            case 'list':
                $step = 15;
                $category = a('category', ['identity' => $module]);
                //构造模块Post的文章显示顺序 默认为日期 倒序
                if(isset($display))
                {
                    //默认后台管理显示文章显示顺序为 pub_time
                    switch ($display->listSort){
                        case 'date':
                            $orderby = 'pub_time';
                            break;
                        case 'initial':
                            $orderby = 'title';
                            break;
                        default:
                            $orderby = 'pub_time';
                    }
                    $order_section = $display->listOrder?:'desc';
                }
                $posts = those('post')->whose('category')->is($category)->orderBy($orderby,$order_section);
                $vars['pagination'] = \Gini\Module\Help::pagination($posts, $form['st'], $step);
                $vars['posts'] = $posts;
                break;
            case 'tag':
                $step = 15;
                $category = a('category', ['identity' => $module]);
                $tags = those('tag')->whose('category')->is($category);
                $vars['pagination'] = \Gini\Module\Help::pagination($tags, $form['st'], $step);
                $vars['tags'] = $tags;
                break;
            default:
                break;
        }
      if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U($view,$vars));
      }else{
        $this->view->body = U('admin/index')
          ->set('content', U($view, $vars));
      }
    }
    function actionSet($vars = [])
    {
        if (!_G('ME')->isAllowedTo('管理')) $this->redirect('error/401');
        $form = $this->form();

        if($form['config']=='modules'){
            $modules = _G('MODS');

            $vars = [
                'form' => $form,
                'index' => $index,
                'modules' => $modules
            ];
        }
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', U('set/'.$form['config'], $vars));
    }

}
