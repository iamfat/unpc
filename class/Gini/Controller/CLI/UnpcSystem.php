<?php

namespace Gini\Controller\CLI;

class UnpcSystem extends \Gini\Controller\CLI {

    function __index($args) 
    {
        echo "Available commands:\n";
        echo "  gini UnpcSystem addUser\n";
    }

    function actionAddUser($args) 
    {
        if (count($args) == 0) {
            die("Usage: gini UnpcSystem addUser [username]\n");
        }

        $username = $args[0];

        echo "Username: $username\n";

        $password = '83719730';
        $email = 'support@geneegroup.com';

        $name = readline('Name: ');

        $username = \Gini\Auth::normalize($username);

        $user = a('user', ['username'=>$username]);
        $user->name = $name;
        $user->username = $username;
        $user->email = $email;
        $user->admin = 1;
        $user->gender = 0;
        $user->save();


        $auth = \Gini\IoC::construct('\Gini\Auth', $username);
        $auth->create($password);

    }

    function actionInit()
    {
        if (!a('category', ['identity' => 'equipment'])->id) {
            echo "Save Default Equipment Module...\n";
            $module = a('category');
            $module->name = T(\Gini\Config::get('system.equipment_category') ?: '仪器列表');
            $module->identity = 'equipment';
            $module->weight = 0;
            $module->icon = 'fa-cog';
            $module->save();
        }

        echo "Update data dir...\n";
        exec(sprintf("chown -R www-data:www-data %s", APP_PATH.'/'.DATA_DIR.'/'));
        $layout_dir = APP_PATH.'/'.DATA_DIR.'/layout/';
        $post_dir = APP_PATH.'/'.DATA_DIR.'/post/';

        exec(sprintf("mkdir -m 775 %s", $layout_dir));
        exec(sprintf("mkdir -m 775 %s", $post_dir));

        exec(sprintf("chown -R www-data:www-data %s", $layout_dir));
        exec(sprintf("chown -R www-data:www-data %s", $post_dir));
        
        $this->_syncEqs();
    }

    private function _syncEqs() {
        try {
            $sys = (array)Hub('config.design.system');
            $apiUrl = $sys['url'];
            if (!$apiUrl) throw new \Exception("Error api url!");
            $rpc = \Gini\Ioc::construct('\Gini\RPC', $apiUrl);
            $result = $rpc->equipment->searchEquipments();
            $apiSelector = $result['token'];

            $equipments = thoseIndexed('equipment', []);
            $total = $equipments->totalCount();
            $start = 0;
            $step = 100;
            while ($total > $start * $step) {
                $eqs = $equipments->limit($start, $step);
                foreach ($eqs as $e) {
                    $this->_saveEqORM($e['id']);
                }
                $start++;
            }
        } catch(\Exception $e){
            echo $e->getMessage(), "\n";
        }
    }

    private function _saveEqORM($id) {
        $equipment = a('equipment', ['rid' => (int)$id]);
        if (!$equipment->id) {
            $equipment->rid = (int)$id;
            $equipment->frontShow = 1;
            $equipment->save();
        }
    }

    public function actionUpdateStyle()
    {
        //更新layout中的图片信息 TODO 
        $data_src = APP_PATH.'/'.DATA_DIR.'/layout/';
        $assert_src = APP_PATH.'/'.RAW_DIR.'/assets/layout/';
        $dist_src = APP_PATH.'/web/assets/layout/';

        exec(sprintf('rm -rf %s', $dist_src));
        exec(sprintf('rm -rf %s', $assert_src));

        exec(sprintf('mkdir -m 775 %s', $data_src));
        exec(sprintf('mkdir -m 775 %s', $assert_src));
        exec(sprintf('mkdir -m 775 %s', $dist_src));

        exec(sprintf("chown -R www-data:www-data %s", $data_src));
        exec(sprintf("chown -R www-data:www-data %s", $assert_src));
        exec(sprintf("chown -R www-data:www-data %s", $dist_src));

        exec(sprintf('cp -r %s %s', $data_src.'*', $assert_src));
        exec(sprintf('cp -r %s %s', $data_src.'*', $dist_src));
    }

}